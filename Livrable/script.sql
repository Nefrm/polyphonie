create table evenement (
id_evenement int not null,
titre varchar(100),
contenu text,
constraint evenement_pk primary key (id_evenement)
);

create table typedocument (
id_type_document int not null,
libelle varchar,
constraint typedocument_pk primary key (id_type_document)
);

create table document (
id_document int not null,
libelle varchar(50),
doc varchar(100),
id_type_document int,
constraint document_pk primary key (id_document),
constraint typedocument_fk foreign key (id_type_document) references typedocument(id_type_document)
);

create table login (
login varchar(50) not null,
password varchar(100) not null,
constraint login_pk primary key (login)
);

create table role (
id_role int not null,
libelle varchar(50),
constraint role_pk primary key (id_role)
);

create table classe (
id_classe int not null,
libelle varchar(50),
matiere varchar(50),
constraint classe_pk primary key (id_classe)
);

create table utilisateur (
id_utilisateur int not null,
id_role int not null,
login varchar(50),
nom varchar(50),
prenom varchar(50),
datenaissance date,
mail varchar(100),
constraint utilisateur_pk primary key (id_utilisateur)
);


create table message (
id_message int not null,
titre varchar(50),
corps text,
date_message date,
constraint message_pk primary key (id_message)
);


create table salle (
id_salle int not null,
numero varchar(15),
constraint salle_pk primary key (id_salle)
);

create table reservation (
id_reservation int not null,
id_salle int,
id_classe int,
date_reservation date,
horaire varchar(50),
duree int,
constraint reservation_pk primary key (id_reservation),
constraint salle_fk foreign key (id_salle) references salle(id_salle),
constraint class_fk foreign key (id_classe) references classe(id_classe)
);

--archive
CREATE TABLE archive_utilisateur (
	id int NOT NULL,
	adresse varchar NULL,
	nom varchar NULL,
	prenom varchar NULL,
	mail varchar NULL,
	datenaissance date null,
	id_role int null,
	CONSTRAINT archive_utilisateur_pk PRIMARY KEY (id),
	CONSTRAINT archive_utilisateur_fk FOREIGN KEY (id_role) REFERENCES public."role"(id_role)
);


insert into role(id_role, libelle)values(1,'admin');
insert into role(id_role, libelle)values(2,'eleve');
insert into role(id_role, libelle)values(3,'professeur');


create sequence seq_archive_utilisateur
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	CACHE 1
	NO CYCLE;

ALTER SEQUENCE seq_archive_utilisateur OWNER TO "admin";
GRANT ALL ON SEQUENCE seq_archive_utilisateur TO "admin";

CREATE OR REPLACE function archivage() returns trigger AS $archive$
	begin
		insert into archive_utilisateur(id, nom, prenom, datenaissance, mail, adresse, id_role) values (nextval('seq_archive_utilisateur'),old.nom, old.prenom, old.date_naissance, old.mail, old.adresse, old.id_role);
		return old;
	end
$archive$ LANGUAGE plpgsql;

CREATE TRIGGER archiver before DELETE ON utilisateur FOR EACH ROW EXECUTE FUNCTION archivage();