package fr.afpa.polyphonie.interfaces.services;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Salle;

public interface ISalleMetier {
	/**
	 * Service metier pour enregistrer une salle dans la base de données
	 * @param salle	: la nouvelle salle à enregistrer
	 * @return	: la salle enregistré
	 */
	public Salle createSalle(Salle salle);
	
	/**
	 * Service metier pour retourner un eliste de salles de cours
	 * @return	: Une liste de salles de cours
	 */
	public List<Salle> getAll();
	
	/**
	 * Service metier qui retourne une salle via sont identifiant
	 * @param id	: L'id de la salle de cours
	 * @return	la salle qui à l'id demandée
	 */
	public Salle getSalle(int id);
	
	/**
	 * Service metier qui modifi une salle
	 * @param salle	: la salle à amodifier
	 * @return	la salle de cours modifier
	 */
	public Salle updateSalle(Salle salle);

	/**
	 * Service metier qui supprime une salle
	 * @param salle	: la salle de cours à supprimer
	 * @return	la salle de cours supprimée 
	 */
	public Salle deleteSalle(int id);
}
