package fr.afpa.polyphonie.interfaces.services;

import java.util.List;
import fr.afpa.polyphonie.entite.metier.Authentification;
import fr.afpa.polyphonie.services.AuthentificationMetier;

public interface IAuthentificationMetier {

	List<Authentification> listeAuthentification();

	boolean authentification(Authentification authentification);

}
