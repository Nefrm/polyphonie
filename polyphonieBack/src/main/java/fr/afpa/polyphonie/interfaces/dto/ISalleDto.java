package fr.afpa.polyphonie.interfaces.dto;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Salle;

public interface ISalleDto {

	/**
	 * Methode pour retourner une liste de salles
	 * @return une liste des salles dans la BDD
	 */
	public List<Salle> getAllSalles();
	
	/**
	 * Methode qui crée une salle
	 * @param salle
	 * @return une salle crée
	 */
	public Salle createSalle(Salle salle);
	
	/**
	 * Methode qui recup�re une salle via son id	
	 * @param id	: id de la salle rechercher
	 * @return	la salle
	 */
	public Salle getSalle(int id);
	
	/**
	 * Methode qui met a jour une salle 
	 * @param salle : la salle a modifier
	 * @return
	 */
	public Salle updateSalle(Salle salle );
	
	/**
	 * Methode qui supprime une salle
	 * @param : id de la salle de cours à supprimer
	 * @return : la salle supprimée
	 */
	public Salle deleteSalle(int id);
	
}
