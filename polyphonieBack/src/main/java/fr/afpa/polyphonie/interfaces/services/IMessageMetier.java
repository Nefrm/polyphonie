package fr.afpa.polyphonie.interfaces.services;

import java.util.Map;

import fr.afpa.polyphonie.entite.metier.Message;

public interface IMessageMetier {
	
	Map<Integer, Message> listerMessage();
	
	boolean creerMessage(Message msg,int expediteur);

	boolean supprimerMessage(int i);


}
