package fr.afpa.polyphonie.interfaces.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.polyphonie.entite.dao.AuthentificationDao;

@Repository
public interface AuthentificationRepository extends JpaRepository<AuthentificationDao, String> {

	public AuthentificationDao findByLogin(String login);
	
	public AuthentificationDao findByLoginAndMdp(String login, String mdp);
	
}
