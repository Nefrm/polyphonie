package fr.afpa.polyphonie.interfaces.dto;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Matiere;

public interface IMatiereDto {
	
	/**
	 * Methode pour retourner une liste de classes
	 * @return: la liste des classes
	 */
	public List<Matiere> getAllClasse();
	
	/**
	 * Methode qui crée une classe
	 * @param classe
	 * @return:	une classe à enregistrer dans la base de données
	 */
	public Matiere createMatiere(Matiere classe);
	
	/**
	 * Service qui récupère la classe via son id
	 * @param id : identifiant de la classe
	 * @return	: la classe correspondante à l'id
	 */
	public Matiere getMatiere(int id);
	
	/**
	 * Service pour modifier l a classe 
	 * @param classe : la classe a modifier 
	 * @return	la classe modifier
	 */
	public Matiere updateMatiere(Matiere classe);
	
	/**
	 * Methode qui supprime une classe
	 * @param id de la classe à supprimer
	 * @return	: la classe supprimé
	 */
	public Matiere deleteSalle(int id);
}
