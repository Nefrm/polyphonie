package fr.afpa.polyphonie.interfaces.services;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Reservation;

public interface IReservationMetier {

	/**
	 * Service metier pour sauvegarder la reservation d'un cours
	 * 
	 * @param reservation : la reservation a sauvegarder
	 * @return la reservation sauvegardee
	 */
	public Reservation createReservation(Reservation reservation);

	/**
	 * Service metier pour recuperer une reservation de cours via l'id
	 * 
	 * @param id : l'id de la reservation
	 * @return : la reservation du cours corespondante a l'id
	 */
	public Reservation getById(int id);

	/**
	 * Service metier pour la modification d'une r�servation de cours
	 * 
	 * @param reservation : la reservationde cours a modifier
	 * @return la reservation de cours modifiee et sauvegardee
	 */
	public Reservation update(Reservation reservation);

	/**
	 * Service pour retourner l'ensemble des reservations de cours sous fome de page
	 * (le nombre de reservation par page est configurable dans la classe
	 * Parametrage)
	 * 
	 * @param page         : le nombre de page
	 * @param nbReservPage :le nombre de reservations de cours par pages
	 * @return une liste correspondant aux reservations a afficher
	 */
	public List<Reservation> getAll(int page, int nbReservPage);

	/**
	 * Service qui retourne le nombre de page necessaire pour afficher les
	 * reservations de cours
	 * 
	 * @param nbReservPage : le nombre de page pour afficher les reservations de
	 *                     cours
	 * @return le nombre de page pour afficher les reservations de cours
	 */
	public int nbPageListeReservation(int nbReservPage);

	/**
	 * Service qui retourne une reservation de cours via son id
	 * 
	 * @param id : l'id de la reservation de cours
	 * @return la reservation de cours
	 */
	public Reservation deleteById(int id);

	/**
	 * Service qui retourne une liste de reservation de cours
	 * 
	 * @return une liste de reservation de cours
	 */
	public List<Reservation> getAll();
}
