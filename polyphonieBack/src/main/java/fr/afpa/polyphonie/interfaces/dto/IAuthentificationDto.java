package fr.afpa.polyphonie.interfaces.dto;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Authentification;
import fr.afpa.polyphonie.services.AuthentificationMetier;

public interface IAuthentificationDto {

	List<Authentification> listeAuthentification();

	boolean authentification(Authentification authentification);

}
