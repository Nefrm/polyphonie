package fr.afpa.polyphonie.interfaces.services;

import java.util.Map;

import fr.afpa.polyphonie.entite.metier.Utilisateur;

public interface IUtilisateurMetier {

	Map<Integer, Utilisateur> listeUtilisateur();

	boolean ajoutUtilisateur(Utilisateur user);

	Utilisateur listeUtilisateur(String choix);

	boolean supprimerUtilisateur(int choix);

	boolean updateUtilisateur(Utilisateur user);

}
