package fr.afpa.polyphonie.interfaces.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.polyphonie.entite.dao.RoleDAO;

@Repository
public interface RoleRepository extends JpaRepository<RoleDAO, Long> {

	public List<RoleDAO> findByLibelle(String libelle);

}
