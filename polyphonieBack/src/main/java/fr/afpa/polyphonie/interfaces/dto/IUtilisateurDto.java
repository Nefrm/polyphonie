package fr.afpa.polyphonie.interfaces.dto;

import java.util.Map;

import fr.afpa.polyphonie.entite.metier.Utilisateur;

public interface IUtilisateurDto {

	Map<Integer, Utilisateur> listeUtilisateur();

	boolean ajoutUtilisateur(Utilisateur user);

	Utilisateur listeUtilisateur(String choix);

	boolean supprimerUtilisateur(int choix);

	boolean updateUtilisateur(Utilisateur user);

}
