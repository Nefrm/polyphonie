package fr.afpa.polyphonie.interfaces.repositories;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.afpa.polyphonie.entite.dao.ReservationDao;

@Repository
public interface ReservationRepository extends JpaRepository<ReservationDao, Integer>{
	
//	@Query("SELECT r FROM ReservationDao r WHERE r.date >= ?1")
//	Page<ReservationDao> findByDate(LocalDate now, Pageable sorteByDate);
}
