package fr.afpa.polyphonie.interfaces.services;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Matiere;

public interface IMatiereMetier {
	/**
	 * Service metier pour enregistrer une classe dans la base de données
	 * @param classe	: la nouvelle classe à enregistrer
	 * @return	: la classe enregistré
	 */
	public Matiere createClasse(Matiere classe);
	
	/**
	 * Sercive métier pour retourner une liste de classes dans la base de données
	 * @return	: la liste de toutes les classes
	 */
	public List<Matiere> getAllClasse();
	
	/**
	 * Service métier pour retourner une seul classe via son identifiant
	 * @param id	: identifiant de la classe sélectionnée
	 * @return	: la classe correspondante à l'id
	 */
	public Matiere getClasse(int id);
	
	/**
	 * Service pour modifier une classe qu'on récupère via sonidentifiant
	 * @param classe	: la classe à modifier
	 * @return	: la classe modifié
	 */
	public Matiere updateClasse(Matiere classe);

	/**
	 * Service métier qui supprime une classe
	 * @param classe	: la classe a supprimer
	 * @return	: la classe supprimée
	 */
	public Matiere DeleteClasse(int id);
}
