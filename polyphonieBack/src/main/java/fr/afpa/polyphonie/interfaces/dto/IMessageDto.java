package fr.afpa.polyphonie.interfaces.dto;

import java.util.Map;

import fr.afpa.polyphonie.entite.metier.Message;

public interface IMessageDto {
	
	Map<Integer,Message> listerMessage();
	
	boolean creerMessage(Message msg,int expediteur);

	boolean supprimerMessage(int choix); 

	

}
