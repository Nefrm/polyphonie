package fr.afpa.polyphonie.interfaces.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.polyphonie.entite.dao.DocumentDAO;

@Repository
public interface DocumentRepository extends JpaRepository<DocumentDAO, Integer> {

}
