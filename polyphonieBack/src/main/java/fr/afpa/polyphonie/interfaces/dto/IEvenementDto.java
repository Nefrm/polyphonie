package fr.afpa.polyphonie.interfaces.dto;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Evenement;

public interface IEvenementDto {

	public Evenement createEvenement(Evenement evenement);

	public Evenement getEvenementById(Long id);

	public Evenement updateEvenement(Evenement evenement);

	public List<Evenement> getAllEvenement();

	public void deleteEvenement(Long id);
}
