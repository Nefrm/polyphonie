package fr.afpa.polyphonie.interfaces.services;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Document;
import fr.afpa.polyphonie.entite.transfert.TransfertDocument;

public interface IDocumentMetier {

	Document createDocument(Document document);

	Document getDocument(String choix);

	boolean deleteDocument(String choix);

	List<Document> getAllDocument();

	List<TransfertDocument> listeDocument();

	boolean contient(Document document);

}
