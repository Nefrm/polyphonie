package fr.afpa.polyphonie.interfaces.dto;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Document;
import fr.afpa.polyphonie.entite.transfert.TransfertDocument;

public interface IDocumentDto {

	Document createDocument(Document document);

	Document getDocument(String choix);

	boolean deleteDocument(String choix);

	List<Document> getAllDocuments();

	List<TransfertDocument> listeDocument();

	boolean contient(Document document);

}
