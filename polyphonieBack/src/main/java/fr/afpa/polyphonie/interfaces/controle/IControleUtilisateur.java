package fr.afpa.polyphonie.interfaces.controle;

import fr.afpa.polyphonie.entite.transfert.CreationUtilisateur;
import fr.afpa.polyphonie.entite.transfert.ModificationUtilisateur;

public interface IControleUtilisateur {

	boolean verificationInfosUtilisateur(CreationUtilisateur utilisateur);

	boolean verificationInfosUtilisateur(ModificationUtilisateur utilisateur);

}
