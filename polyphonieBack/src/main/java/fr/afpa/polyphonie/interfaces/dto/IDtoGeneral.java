package fr.afpa.polyphonie.interfaces.dto;

import fr.afpa.polyphonie.entite.dao.AuthentificationDao;
import fr.afpa.polyphonie.entite.dao.DocumentDAO;
import fr.afpa.polyphonie.entite.dao.EvenementDao;
import fr.afpa.polyphonie.entite.dao.MessageDAO;
import fr.afpa.polyphonie.entite.dao.UtilisateurDAO;
import fr.afpa.polyphonie.entite.metier.Authentification;
import fr.afpa.polyphonie.entite.metier.Document;
import fr.afpa.polyphonie.entite.metier.Evenement;
import fr.afpa.polyphonie.entite.metier.Message;
import fr.afpa.polyphonie.entite.metier.Utilisateur;

public interface IDtoGeneral {

	Utilisateur utilisateurDAOToUtilisateurDTO(UtilisateurDAO utilisateurDAO);

	Authentification authentificationDAOToAuthentification(AuthentificationDao authentificationDao);

	UtilisateurDAO utilisateurToUtilisateurDAO(Utilisateur user);

	AuthentificationDao authentificationToAuthentificationDAO(Authentification authentification);

	EvenementDao mappingEvenementToEvenementDao(Evenement evenement);
	Evenement mappingEvenementDaoToEvenement(EvenementDao evenementDao);

	Message messageDAOToMessageDTO(MessageDAO messageDAO);

	MessageDAO messageToMessageDAO(Message msg);

	DocumentDAO documentToDocumentDAO(Document document);

	Document documentDAOToDocument(DocumentDAO documentDAO);
}
