package fr.afpa.polyphonie.interfaces.dto;

import java.util.List;

import fr.afpa.polyphonie.entite.metier.Reservation;



public interface IReservationDto {
	
	/**
	 * dto pour la sauvegarde d'une reservation de cours
	 * @param reservation	: la reservation du cours a sauvegarder
	 * @return la reservation du cours sauvegardee
	 */
	public Reservation createReservation(Reservation reservation);
	
	/**
	 * dto pour recuperer une reservation de cours via l'id de la reservation
	 * @param id	: l'id de la reservation du cours
	 * @return	: la reservation du cours correspondante à l'id
	 */
	public Reservation getById(int id);
	
	/**
	 * dto pour la modification d'une reservation de cours
	 * @param reservation	: la reservation du cours à modifier
	 * @return	la reservation du cours modifiee qui a ete sauvegardee
	 */
	public Reservation update(Reservation reservation);
	
	/**
	 * dto pour retourner l'ensemble des reservations de cours sous forme de page 
	 * @param page	: le nombre de page 
	 * @param nbReservPage	: le nombre de reservation de cours a afficher
	 * @return	une liste correspondant aux reservation a afficher
	 */
	public List<Reservation> getAll(int page , int nbReservPage);
	
	/**
	 * dto qui retourne le nombre de page necessaire pour afficher les reservations de cours
	 * @param nbReservPage	: la nombre de reservation de cours par page
	 * @return le nombre de page pour afficher les reservation de cours
	 */
	public int nbPageListeReservation(int nbReservPage);
	
	/**
	 * Supprime une reservation de cours via son id
	 * @param id	: l'id de la reservation a supprimer 
	 * @return	: la reservation de cours supprimee
	 */
	public Reservation deleteById(int id);
	
	/**
	 * dto qui retourne une liste de reservation de cours 
	 * @return une liste de reservation de cours
	 */
	public List<Reservation> getAll();
	
	
}
