package fr.afpa.polyphonie.entite.dao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of= {"login","mdp"})

@Entity
@Table(name="authentification")
public class AuthentificationDao {
	
	public AuthentificationDao(String login, String mdp) {
		this.login = login;
		this.mdp = mdp;
	}

	@Id
	@Column
	private String login;
	
	@Column
	private String mdp;
	
	@OneToOne(mappedBy="authentification", cascade=CascadeType.PERSIST, orphanRemoval = true, fetch = FetchType.EAGER)
	@OnDelete( action = OnDeleteAction.CASCADE )
	private UtilisateurDAO personne;
}
