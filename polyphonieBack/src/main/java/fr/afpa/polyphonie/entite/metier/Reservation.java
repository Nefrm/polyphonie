package fr.afpa.polyphonie.entite.metier;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Reservation {
	
	
	int id;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	LocalDate date;
	String horaire;
	int durer;
	Matiere matiere;
	Salle salle;
	
	public Reservation(int id, LocalDate date, String horaire, int durer) {
		super();
		this.id = id;
		this.date = date;
		this.horaire = horaire;
		this.durer = durer;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Reservation) {
			return this.getId()==((Reservation)obj).getId();
		}
		return false;
	}
	@Override
	public int hashCode() {
		return this.getId();
	}
}
