package fr.afpa.polyphonie.entite.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter

@Entity
@Table(name="document")
public class DocumentDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "document_generator")
	@SequenceGenerator(name = "document_generator", sequenceName = "seq_document",allocationSize = 1,initialValue = 1)
	private Integer id_document;
	
	@Column
	private String document;
}
