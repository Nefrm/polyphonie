package fr.afpa.polyphonie.entite.metier;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Salle {

	int id;
	String numero;
	List <Reservation> listReservation;
	
	public Salle(int id, String numero) {
		super();
		this.id = id;
		this.numero = numero;
	}
}
