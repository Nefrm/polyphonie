package fr.afpa.polyphonie.entite.metier;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Matiere {

	Integer idMatiere;
	String libelle;
	Utilisateur professeur;
	List<Utilisateur> listeEleves;
	List<Reservation> listeReservations;
	List<Note> notes;
	String appreciation;

	public Matiere(Integer idMatiere, String libelle) {
		super();
		this.idMatiere = idMatiere;
		this.libelle = libelle;
	}
}
