package fr.afpa.polyphonie.entite.metier;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Utilisateur {
	private int id;
	private List<Message> messages;
	private boolean actif;
	private String nom;
	private String prenom;
	private String adresse;
	private LocalDate dateNaissance;
	private String mail;
	private List<Matiere> classes;
	private Role role;
	private Authentification authentification;
}
