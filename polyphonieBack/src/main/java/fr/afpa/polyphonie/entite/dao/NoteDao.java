package fr.afpa.polyphonie.entite.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter

@Entity
@Table(name="note")
public class NoteDao {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "note_generator")
	@SequenceGenerator(name = "note_generator", sequenceName = "seq_note",allocationSize = 1,initialValue = 1)
	private int id_note;
	
	@Column
	private float note;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	private MatiereDao matiere;
}
