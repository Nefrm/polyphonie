package fr.afpa.polyphonie.entite.dao;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "salle")
public class SalleDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "salle_generator")
	@SequenceGenerator(name = "salle_generator", sequenceName = "seq_salle", allocationSize = 1, initialValue = 1)
	int id_salle;

	@Column
	String numero;

	@OneToMany(mappedBy = "salle")
	@OnDelete(action = OnDeleteAction.CASCADE)
	List<ReservationDao> listeReservationDao;

	public SalleDao(int id_salle, String numero) {
		super();
		this.id_salle = id_salle;
		this.numero = numero;
	}

}
