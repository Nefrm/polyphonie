package fr.afpa.polyphonie.entite.dao;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter

@Entity
@Table(name = "reservation")
public class ReservationDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservation_generator")
	@SequenceGenerator(name = "reservation_generator", sequenceName = "seq_reservation", allocationSize = 1, initialValue = 1)
	@Column(name = "id_reservation", updatable = false, nullable = false)
	int id_reservation;

	@Column(name = "date_reservation", updatable = true, nullable = false)
	LocalDate date_reservation;

	@Column(name = "heure_debut", updatable = true, nullable = false)
	String horaire;

	@Column(name = "duree", updatable = true, nullable = false)
	int duree;

	@ManyToOne(cascade = { CascadeType.PERSIST }, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_matiere")
	MatiereDao matiere;

	@ManyToOne(cascade = { CascadeType.PERSIST }, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_salle")
	SalleDao salle;

	public ReservationDao(int id_reservation, LocalDate date_reservation, String horaire, int duree) {
		super();
		this.id_reservation = id_reservation;
		this.date_reservation = date_reservation;
		this.horaire = horaire;
		this.duree = duree;
	}

}
