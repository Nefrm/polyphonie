package fr.afpa.polyphonie.entite.transfert;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ModificationUtilisateur {

	private String id;
	private String nom;
	private String prenom;
	private String mail;
	private String adresse;
	private String role;
	private String dateNaissance;
}
