package fr.afpa.polyphonie.entite.dao;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="utilisateur")
public class UtilisateurDAO {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "utilisateur_generator")
	@SequenceGenerator(name="utilisateur_generator", sequenceName = "seq_utilisateur", allocationSize=1,initialValue = 2)
	private int id_utilisateur;
	
	@Column
	private String nom;
	
	@Column
	private String prenom;
	
	@Column
	private Date dateNaissance;
	
	@Column
	private String mail;
	
	@Column
	private String adresse;
	
	@ManyToOne
	@JoinColumn(name="id_matiere")
	private MatiereDao matiere;
	
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "login")
	private AuthentificationDao authentification;

	@ManyToOne
	@JoinColumn(name="id_role")
	private RoleDAO role;
	
	@OneToMany
	private List<MatiereDao> pMatiere;
	
	@ManyToMany
	@JoinColumn(name = "utilisateurs")
	private List<MatiereDao> matieres;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "utilisateurs")
	private List<MessageDAO> messages;
	
	@OneToMany(mappedBy = "expediteur", cascade = CascadeType.ALL)
	private List<MessageDAO> message;
}
