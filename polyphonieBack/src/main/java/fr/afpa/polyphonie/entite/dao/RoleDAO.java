package fr.afpa.polyphonie.entite.dao;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name="role")
public class RoleDAO {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "role_generator")
	@SequenceGenerator(name="role_generator", sequenceName = "seq_role", allocationSize=1,initialValue = 1)
	private int id_role;
	
	@Column
	private String libelle;
	
	@JsonIgnore
	@OneToMany(mappedBy = "role")
	private List<UtilisateurDAO> utilisateur;
}
