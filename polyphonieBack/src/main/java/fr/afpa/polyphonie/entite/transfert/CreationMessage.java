package fr.afpa.polyphonie.entite.transfert;

import java.time.format.DateTimeFormatter;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CreationMessage {
	
	private String titre;
	private String corps;
	private String date;
	
	private int expediteur;
	

}
