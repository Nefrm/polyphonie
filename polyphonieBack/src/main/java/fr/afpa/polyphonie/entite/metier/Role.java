package fr.afpa.polyphonie.entite.metier;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Role {
	private int id;
	private String libelle;
	
	public Role(String libelle) {
		this.libelle = libelle;
	}
}
