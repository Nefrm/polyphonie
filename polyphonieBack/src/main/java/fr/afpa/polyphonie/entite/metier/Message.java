package fr.afpa.polyphonie.entite.metier;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class Message {
	
	 Long id;
	 String titre;
	 String corps;
	 LocalDate date;
	 Boolean archiveExp;
	 Boolean archiveDest;
	
	 List<Utilisateur> listeDestinataires;
	
	 Utilisateur expediteur;
	
	

}
