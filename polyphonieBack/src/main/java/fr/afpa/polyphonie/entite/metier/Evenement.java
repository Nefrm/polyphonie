package fr.afpa.polyphonie.entite.metier;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class Evenement {

	private Long id_evenement;
	private String titre;
	private String contenu;
	private String image;
	private MultipartFile imageUploaded;
}
