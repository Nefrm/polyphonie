package fr.afpa.polyphonie.entite.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter

@Entity
@Table(name = "evenement")
public class EvenementDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evenement_generator")
	@SequenceGenerator(name = "evenement_generator", sequenceName = "seq_evenement", allocationSize = 1, initialValue = 1)
	private Long id_evenement;
	private String titre;
	private String contenu;
	private String image;
}
