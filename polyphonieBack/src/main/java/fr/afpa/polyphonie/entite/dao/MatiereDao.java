package fr.afpa.polyphonie.entite.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter

@Entity
@Table(name="matiere")
public class MatiereDao {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "matiere_generator")
	@SequenceGenerator(name = "matiere_generator", sequenceName = "seq_matiere",allocationSize = 1,initialValue = 1)
	private int id_matiere;
	
	@Column
	private String libelle;

	@Column
	private String appreciation;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	private UtilisateurDAO professeur;
	
	@ManyToMany(mappedBy = "matiere")
	private List<UtilisateurDAO> listeElevesDao;
	
	@OneToMany(mappedBy = "matiere")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<ReservationDao> listeReservationDao;
	
	@OneToMany(mappedBy = "matiere")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<NoteDao> listeNoteDao;

	public MatiereDao(int id_matiere, String libelle) {
		super();
		this.id_matiere = id_matiere;
		this.libelle = libelle;
	}
}
