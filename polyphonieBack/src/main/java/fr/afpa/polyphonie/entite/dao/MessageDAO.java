package fr.afpa.polyphonie.entite.dao;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "message")
public class MessageDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "message_generator")
	@SequenceGenerator(name = "message_generator", sequenceName = "seq_message", allocationSize = 1, initialValue = 1)
	private Integer id_message;

	@Column
	private String titre;

	@Column
	private String corps;

	@Column
	private LocalDate date;

	@Column
	private Boolean archiveExp;

	@Column
	private Boolean archiveDest;

//	@OneToMany(mappedBy = "message", cascade = CascadeType.ALL)
//	private List<UtilisateurMessageDAO> listUserMess;

	@ManyToMany(mappedBy = "messages")
	private List<UtilisateurDAO> destinataires;

	@ManyToOne
	@JoinColumn(name = "expediteur")
	private UtilisateurDAO expediteur;

}
