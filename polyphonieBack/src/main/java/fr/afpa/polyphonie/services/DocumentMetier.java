package fr.afpa.polyphonie.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.metier.Document;
import fr.afpa.polyphonie.entite.transfert.TransfertDocument;
import fr.afpa.polyphonie.interfaces.dto.IDocumentDto;
import fr.afpa.polyphonie.interfaces.services.IDocumentMetier;

@Service
public class DocumentMetier implements IDocumentMetier {

	@Autowired
	private IDocumentDto documentDto;
	
	@Override
	public Document createDocument(Document document) {
		return documentDto.createDocument(document);
	}

	@Override
	public Document getDocument(String choix) {
		return documentDto.getDocument(choix);
	}

	@Override
	public boolean deleteDocument(String choix) {
		return documentDto.deleteDocument(choix);
	}

	@Override
	public List<Document> getAllDocument() {
		return documentDto.getAllDocuments();
	}

	@Override
	public List<TransfertDocument> listeDocument() {
		return documentDto.listeDocument();
	}

	@Override
	public boolean contient(Document document) {
		return documentDto.contient(document);
	}

}
