package fr.afpa.polyphonie.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.metier.Reservation;
import fr.afpa.polyphonie.interfaces.dto.IReservationDto;
import fr.afpa.polyphonie.interfaces.services.IReservationMetier;

@Service
public class ReservationMetier implements IReservationMetier {

	@Autowired
	private IReservationDto iServReservationDto;

	/**
	 * Methode pour crée un reservation à enregistrer
	 */
	@Override
	public Reservation createReservation(Reservation reservation) {
		return iServReservationDto.createReservation(reservation);
	}

	/**
	 * Methode pour trouver une reservation selin son id
	 */
	@Override
	public Reservation getById(int id) {
		return iServReservationDto.getById(id);
	}

	/**
	 * Methode pour modifier une reservation
	 */
	@Override
	public Reservation update(Reservation reservation) {
		return iServReservationDto.update(reservation);
	}

	/**
	 * Methode pour afficher une liste de reservation avec une pagination
	 */
	@Override
	public List<Reservation> getAll(int page, int nbReservPage) {
		return iServReservationDto.getAll(page, nbReservPage);
	}

	/**
	 * Methode pour le nombre de reservation a afficher dans chaque pages de la
	 * pagination
	 */
	@Override
	public int nbPageListeReservation(int nbReservPage) {
		return iServReservationDto.nbPageListeReservation(nbReservPage);
	}

	/**
	 * Methode pour supprimer une reservation
	 */
	@Override
	public Reservation deleteById(int id) {
		return iServReservationDto.deleteById(id);
	}

	/**
	 * Methode qui retrourne une liste de reservation
	 */
	@Override
	public List<Reservation> getAll() {
		return iServReservationDto.getAll();
	}

}
