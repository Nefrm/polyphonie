package fr.afpa.polyphonie.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.metier.Utilisateur;
import fr.afpa.polyphonie.entite.transfert.TransfertUtilisateur;
import fr.afpa.polyphonie.interfaces.dto.IUtilisateurDto;
import fr.afpa.polyphonie.interfaces.services.IUtilisateurMetier;

@Service
public class UtilisateurMetier implements IUtilisateurMetier {

	@Autowired
	private IUtilisateurDto dtoUtilisateur;

	@Override
	public Map<Integer, Utilisateur> listeUtilisateur() {
		return dtoUtilisateur.listeUtilisateur();
	}

	@Override
	public boolean ajoutUtilisateur(Utilisateur user) {
		return dtoUtilisateur.ajoutUtilisateur(user);
	}

	@Override
	public Utilisateur listeUtilisateur(String choix) {
		return dtoUtilisateur.listeUtilisateur(choix);
	}

	@Override
	public boolean supprimerUtilisateur(int choix) {
		return dtoUtilisateur.supprimerUtilisateur(choix);
	}

	@Override
	public boolean updateUtilisateur(Utilisateur user) {
		return dtoUtilisateur.updateUtilisateur(user);
	}

}
