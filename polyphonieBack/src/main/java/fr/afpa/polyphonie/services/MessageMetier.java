package fr.afpa.polyphonie.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.metier.Message;
import fr.afpa.polyphonie.interfaces.dto.IMessageDto;
import fr.afpa.polyphonie.interfaces.services.IMessageMetier;

@Service
public class MessageMetier implements IMessageMetier{
	

	@Autowired
	private IMessageDto imessageDto;
	
	@Override
	public Map<Integer, Message> listerMessage() {
		return imessageDto.listerMessage();
	}

	@Override
	public boolean creerMessage(Message msg, int expediteur) {
		return imessageDto.creerMessage(msg, expediteur);
	}

	@Override
	public boolean supprimerMessage(int choix) {
		
		return imessageDto.supprimerMessage(choix);
	}

}
