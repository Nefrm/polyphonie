package fr.afpa.polyphonie.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.metier.Salle;
import fr.afpa.polyphonie.interfaces.services.ISalleMetier;

@Service
public class SalleMetier implements ISalleMetier {
	
	@Autowired
	private ISalleMetier servSalle;
	
	/**
	 * Methode pour creer une salle de cours
	 */
	@Override
	public Salle createSalle(Salle salle) {
		return servSalle.createSalle(salle);
	}
	
	/**
	 * Methode pour recuperer toute les salles de cours
	 */
	@Override
	public List<Salle> getAll() {
		return servSalle.getAll();
	}
	
	/**
	 * Methode pour recuperer une salle de cours via son id
	 */
	@Override
	public Salle getSalle(int id) {
		return servSalle.getSalle(id);
	}

	/**
	 * Methode pour transformer une salle de cours
	 */
	@Override
	public Salle updateSalle(Salle salle) {
		return servSalle.updateSalle(salle);
	}

	@Override
	public Salle deleteSalle(int id) {
		return servSalle.deleteSalle(id);
	}
}