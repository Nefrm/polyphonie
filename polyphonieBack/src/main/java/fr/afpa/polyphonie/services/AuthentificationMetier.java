package fr.afpa.polyphonie.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.metier.Authentification;
import fr.afpa.polyphonie.interfaces.dto.IAuthentificationDto;
import fr.afpa.polyphonie.interfaces.services.IAuthentificationMetier;

@Service
public class AuthentificationMetier implements IAuthentificationMetier {

	@Autowired
	private IAuthentificationDto authentificationDto;
	
	@Override
	public List<Authentification> listeAuthentification() {
		return authentificationDto.listeAuthentification();
	}

	@Override
	public boolean authentification(Authentification authentification) {
		return authentificationDto.authentification(authentification);
	}

}
