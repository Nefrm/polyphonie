package fr.afpa.polyphonie.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.metier.Evenement;
import fr.afpa.polyphonie.interfaces.dto.IEvenementDto;
import fr.afpa.polyphonie.interfaces.services.IEvenementMetier;

@Service
public class EvenementMetier implements IEvenementMetier {

	@Autowired
	private IEvenementDto servEvenement;
	
	@Override
	public Evenement createEvenement(Evenement evenement) {
		// TODO Auto-generated method stub
		return servEvenement.createEvenement(evenement);
	}

	@Override
	public Evenement getEvenementById(Long id) {
		// TODO Auto-generated method stub
		return servEvenement.getEvenementById(id);
	}

	@Override
	public Evenement updateEvenement(Evenement evenement) {
		// TODO Auto-generated method stub
		return servEvenement.updateEvenement(evenement);
	}

	@Override
	public List<Evenement> getAllEvenement() {
		// TODO Auto-generated method stub
		return servEvenement.getAllEvenement();
	}

	@Override
	public void deleteEvenement(Long id) {
		servEvenement.deleteEvenement(id);
		
	}

}
