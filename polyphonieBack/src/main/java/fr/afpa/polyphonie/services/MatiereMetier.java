package fr.afpa.polyphonie.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.metier.Matiere;
import fr.afpa.polyphonie.interfaces.services.IMatiereMetier;

@Service
public class MatiereMetier implements IMatiereMetier{

	@Autowired
	private IMatiereMetier servClasseDto;
	
	@Override
	public Matiere createClasse(Matiere classe) {
		return servClasseDto.createClasse(classe);
	}

	@Override
	public List<Matiere> getAllClasse() {
		return servClasseDto.getAllClasse();
	}

	@Override
	public Matiere getClasse(int id) {
		return servClasseDto.getClasse(id);
	}

	@Override
	public Matiere updateClasse(Matiere classe) {		
		return servClasseDto.updateClasse(classe);
	}

	@Override
	public Matiere DeleteClasse(int id) {
		return servClasseDto.DeleteClasse(id);
	}
}
