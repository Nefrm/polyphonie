package fr.afpa.polyphonie.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.dao.AuthentificationDao;
import fr.afpa.polyphonie.entite.metier.Authentification;
import fr.afpa.polyphonie.interfaces.dto.IAuthentificationDto;
import fr.afpa.polyphonie.interfaces.dto.IDtoGeneral;
import fr.afpa.polyphonie.interfaces.repositories.AuthentificationRepository;
import fr.afpa.polyphonie.services.AuthentificationMetier;

@Service
public class AuthentificationDto implements IAuthentificationDto {

	@Autowired
	private AuthentificationRepository authentificationRepository;
	@Autowired
	private IDtoGeneral DtoGeneral;
	
	@Override
	public List<Authentification> listeAuthentification() {
		List<AuthentificationDao> listeDAO = authentificationRepository.findAll();
		List<Authentification> listeDTO = new ArrayList<Authentification>();
		for (AuthentificationDao authentificationDao : listeDAO) {
			listeDTO.add(DtoGeneral.authentificationDAOToAuthentification(authentificationDao));
		}
		return listeDTO;
	}

	@Override
	public boolean authentification(Authentification authentification) {
		AuthentificationDao auth = authentificationRepository.findByLoginAndMdp(authentification.getLogin(), authentification.getMdp());
		return auth != null;
	}

}
