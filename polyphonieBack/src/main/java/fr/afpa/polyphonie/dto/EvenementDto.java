package fr.afpa.polyphonie.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.dao.EvenementDao;
import fr.afpa.polyphonie.entite.metier.Evenement;
import fr.afpa.polyphonie.interfaces.dto.IDtoGeneral;
import fr.afpa.polyphonie.interfaces.dto.IEvenementDto;
import fr.afpa.polyphonie.interfaces.repositories.EvenementRepository;

@Service
public class EvenementDto implements IEvenementDto{

	@Autowired
	private IDtoGeneral serviceGeneral;
	
	@Autowired
	private EvenementRepository evenementRepo;
	
	@Override
	public Evenement createEvenement(Evenement evenement) {
		// TODO Auto-generated method stub
		EvenementDao evenementDao = serviceGeneral.mappingEvenementToEvenementDao(evenement);
		evenementRepo.save(evenementDao);
		return evenement;
	}

	@Override
	public Evenement getEvenementById(Long id) {
		// TODO Auto-generated method stub
		EvenementDao evenementDao = evenementRepo.findById(id).orElse(null);
		Evenement evenement = serviceGeneral.mappingEvenementDaoToEvenement(evenementDao);
		return evenement;
	}

	@Override
	public Evenement updateEvenement(Evenement evenement) {
		// TODO Auto-generated method stub
		EvenementDao evenementDao = evenementRepo.findById(evenement.getId_evenement()).orElse(null);
		evenementDao = serviceGeneral.mappingEvenementToEvenementDao(evenement);
		evenementRepo.saveAndFlush(evenementDao);
		return evenement;
		
	}

	@Override
	public List<Evenement> getAllEvenement() {
		// TODO Auto-generated method stub
		List<EvenementDao> listEvenementDao = evenementRepo.findAll();
		List<Evenement> listEvenements = new ArrayList<Evenement>();
		for (EvenementDao evenementDao : listEvenementDao) {
			Evenement evenement = serviceGeneral.mappingEvenementDaoToEvenement(evenementDao);
			listEvenements.add(evenement);
		}
	    return listEvenements;
	}

	@Override
	public void deleteEvenement(Long id) {
		evenementRepo.deleteById(id);	
	}

	
}
