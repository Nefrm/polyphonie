package fr.afpa.polyphonie.dto;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.dao.ReservationDao;
import fr.afpa.polyphonie.entite.dao.SalleDao;
import fr.afpa.polyphonie.entite.metier.Reservation;
import fr.afpa.polyphonie.entite.metier.Salle;
import fr.afpa.polyphonie.interfaces.dto.ISalleDto;
import fr.afpa.polyphonie.interfaces.repositories.SalleRepository;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class SalleDto implements ISalleDto {

	@Autowired
	private SalleRepository servSalleRep;
	
	@Override
	@Transactional
	public List<Salle> getAllSalles() {
		List<SalleDao> listeSalleDao = servSalleRep.findAll();
		List<Salle> listeSalle = listeSalleDao.stream().map(SalleDto::salleDaoToSalle)
				.collect(Collectors.toList());
		return listeSalle;
	}
	
	@Override
	@Transactional
	public Salle createSalle(Salle salle) {
		SalleDao salleDao = salleToSalleDao(salle);
		SalleDao salleDaoCreer = null;
		try {
			salleDaoCreer = servSalleRep.save(salleDao);
			return salleDaoToSalle(salleDaoCreer);
		}catch (Exception e){
			Logger.getLogger(SalleDto.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}

	@Override
	@Transactional
	public Salle getSalle(int id) {
		Optional<SalleDao> salleDao = servSalleRep.findById(id);
		return salleDaoToSalle(salleDao.get());
	}

	@Override
	public Salle updateSalle(Salle salle) {
		SalleDao salleDao = salleToSalleDao(salle);
		SalleDao salleDaoUpdate = null;
		try {
			salleDaoUpdate = servSalleRep.save(salleDao);
		}catch (Exception e ) {
			Logger.getLogger(SalleDto.class.getName()).log(Level.SEVERE, null, e);
		}
		return salleDaoToSalle(salleDaoUpdate);
	}
	/**
	 * Methode qui transforme une salleDao (persistante) en salleDto (metier)
	 * @param salleDao	: la salle transformée en SalleDto
	 * @return	SalleDto (metier)
	 */
	public static Salle salleDaoToSalle(SalleDao salleDao) {
		Salle salle = new Salle(salleDao.getId_salle(),salleDao.getNumero());
		List<Reservation> listeReservation = salleDao.getListeReservationDao().stream()
				.map(ReservationDto::reservationDaoToReservation)
				.sorted((r1, r2) -> r1.getDate().compareTo((r2.getDate())))
				.collect(Collectors.toList());
		salle.setListReservation(listeReservation);
		return salle;
	}
	/**
	 * Methode qui transforme une salleDto (metier) en salleDao (persistante)
	 * @param salleDto	: la salle transformée en SalleDao
	 * @return	SalleDao (persistante)
	 */
	public static SalleDao salleToSalleDao(Salle salle) {
		SalleDao salleDao = new SalleDao(salle.getId(),salle.getNumero());
		List<ReservationDao> listeReservationDao = salle.getListReservation().stream()
				.map(ReservationDto::reservationToReservationDao).collect(Collectors.toList());
		salleDao.setListeReservationDao(listeReservationDao);
		return salleDao;
	}
	/**
	 * Methode qui supprime une salle de cours
	 * @param : l'id de la salle de cours à supprimer
	 * @return la salle supprimée
	 */
	@Override
	public Salle deleteSalle(int id) {
		try {
			Salle salle = null;
			Optional<SalleDao> salleDao = servSalleRep.findById(id);
			if(salleDao.isPresent()) {
				salle = salleDaoToSalle(salleDao.get());
				servSalleRep.delete(salleDao.get());
			}
			return salle;
		}catch (Exception e) {
			return null;
		}
	}
}
