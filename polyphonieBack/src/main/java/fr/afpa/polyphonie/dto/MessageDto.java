package fr.afpa.polyphonie.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.dao.AuthentificationDao;
import fr.afpa.polyphonie.entite.dao.MessageDAO;
import fr.afpa.polyphonie.entite.dao.UtilisateurDAO;
import fr.afpa.polyphonie.entite.metier.Message;
import fr.afpa.polyphonie.interfaces.dto.IDtoGeneral;
import fr.afpa.polyphonie.interfaces.dto.IMessageDto;
import fr.afpa.polyphonie.interfaces.repositories.MessageRepository;
import fr.afpa.polyphonie.interfaces.repositories.UtilisateurRepository;

@Service
public class MessageDto implements IMessageDto {

	@Autowired
	private IDtoGeneral dtoGeneral;

	@Autowired
	private MessageRepository messageRepository;

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Override
	public Map<Integer, Message> listerMessage() {
		List<MessageDAO> messagesDAO = messageRepository.findAll();
		Map<Integer, Message> messagesDTO = new HashMap<Integer, Message>();
		for (MessageDAO messageDAO : messagesDAO) {
			messagesDTO.put(messageDAO.getId_message(), dtoGeneral.messageDAOToMessageDTO(messageDAO));
		}
		return messagesDTO;
	}

	@Override
	public boolean creerMessage(Message msg, int expediteur) {
		MessageDAO messageDAO = new MessageDAO();
		messageDAO = dtoGeneral.messageToMessageDAO(msg);
		Optional<UtilisateurDAO> listeUtilisateurDAO = utilisateurRepository.findById(expediteur);
		if (listeUtilisateurDAO.isPresent()) {
			messageDAO.setExpediteur(listeUtilisateurDAO.get());
			messageRepository.save(messageDAO);
			return true;
		}
		return false;
	}

	@Override
	public boolean supprimerMessage(int choix) {
		messageRepository.deleteById(choix);
		return true;
	}

}
