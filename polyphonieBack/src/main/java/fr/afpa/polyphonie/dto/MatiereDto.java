package fr.afpa.polyphonie.dto;



import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import fr.afpa.polyphonie.entite.dao.MatiereDao;
import fr.afpa.polyphonie.entite.dao.ReservationDao;
import fr.afpa.polyphonie.entite.dao.UtilisateurDAO;
import fr.afpa.polyphonie.entite.metier.Matiere;
import fr.afpa.polyphonie.entite.metier.Reservation;
import fr.afpa.polyphonie.entite.metier.Utilisateur;
import fr.afpa.polyphonie.interfaces.dto.IMatiereDto;
import fr.afpa.polyphonie.interfaces.repositories.MatiereRepository;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class MatiereDto implements IMatiereDto {

	@Autowired
	private MatiereRepository servClasseRep;
	
	@Autowired
	private static DtoGeneral DtoGeneral;
	
	/**
	 * Methode pour transformer une classe persistante en classe métier
	 * @param classeDao	:classeDao récupèré dans la base de données
	 * @return	: la classe métier
	 */
	public static Matiere matiereDaoToMatiere(MatiereDao classeDao) {
		Matiere classe = new Matiere(classeDao.getId_matiere(), classeDao.getLibelle());
		classe.setProfesseur(DtoGeneral.utilisateurDAOToUtilisateurDTO(classeDao.getProfesseur()));
		List<Utilisateur> listeEleves = classeDao.getListeElevesDao().stream()
				.map(DtoGeneral::utilisateurDAOToUtilisateurDTO).collect(Collectors.toList());
		List<Reservation> listeReservations = classeDao.getListeReservationDao().parallelStream()
				.map(ReservationDto::reservationDaoToReservation)
				.sorted((r1,r2) -> r1.getDate().compareTo(r2.getDate())).collect(Collectors.toList());
		classe.setListeEleves(listeEleves);
		classe.setListeReservations(listeReservations);
		return classe;
	}
	/**
	 * Methode pour transformer une classe métier en classe persistante
	 * @param matiere : classe métier récupèré à transformer en classe persistante
	 * @return	: la classe persistante
	 */
	public static MatiereDao matiereToMatiereDao(Matiere matiere) {
		MatiereDao classeDao = new MatiereDao(matiere.getIdMatiere(),matiere.getLibelle());
		classeDao.setProfesseur(DtoGeneral.utilisateurToUtilisateurDAO(matiere.getProfesseur()));
		List<UtilisateurDAO> listeElevesDao = matiere.getListeEleves().stream()
				.map(DtoGeneral::utilisateurToUtilisateurDAO).collect(Collectors.toList());
		List<ReservationDao> listeReservationDao = matiere.getListeReservations().stream()
				.map(ReservationDto::reservationToReservationDao).collect(Collectors.toList());
		classeDao.setListeElevesDao(listeElevesDao);
		classeDao.setListeReservationDao(listeReservationDao);
		return classeDao;
	}
	/**
	 * Methode qui retourne une liste de classe present dans la base de données
	 */
	@Override
	@Transactional
	public List<Matiere> getAllClasse() {
		List<MatiereDao> listeClasseDao = servClasseRep.findAll();
		List<Matiere> listeClasse = listeClasseDao.stream().map(MatiereDto::matiereDaoToMatiere)
				.collect(Collectors.toList());
		return listeClasse;
	}
	/**
	 * Methode pour créer une nouvelle classe dans la base de données
	 * @param classe : la classe qui sera transformé et enregistré dans la base de données
	 */
	@Override
	@Transactional
	public Matiere createMatiere(Matiere classe) {
		MatiereDao classeDao = matiereToMatiereDao(classe);
		MatiereDao classeDaoCreer = null;
		classeDao.getListeElevesDao().forEach(e -> e.setMatiere(classeDao));
		classeDao.getListeReservationDao().forEach(e -> e.setMatiere(classeDao));
		try {
			classeDaoCreer = servClasseRep.save(classeDao);
			return matiereDaoToMatiere(classeDaoCreer);
		}catch (Exception e ) {
			Logger.getLogger(MatiereDto.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}
	
	/**
	 * Methode pour récupèrer la classeDao via son id
	 */
	@Override
	@Transactional
	public Matiere getMatiere(int id) {
		Optional<MatiereDao> classeDao = servClasseRep.findById(id);
		return matiereDaoToMatiere(classeDao.get());
	}
	
	/**
	 * Methode pour modifier la classe
	 * @param classe : la classe a modifier
	 */
	@Override
	public Matiere updateMatiere(Matiere classe) {
		MatiereDao classeDao = matiereToMatiereDao(classe);
		classeDao.getListeElevesDao().forEach(e ->e.setMatiere(classeDao));
		classeDao.getListeReservationDao().forEach(e -> e.setMatiere(classeDao));
		MatiereDao classeDaoUpdate = null;
		try {
			classeDaoUpdate = servClasseRep.saveAndFlush(classeDao);
		}catch (Exception e) {
			Logger.getLogger(MatiereDto.class.getName()).log(Level.SEVERE, null , e);
		}
		return matiereDaoToMatiere(classeDaoUpdate);
	}
	/**
	 * Methode qui supprime une classe
	 * @param	: l'id de la classe à supprimer
	 * @return	la classe supprimée
	 */
	@Override
	public Matiere deleteSalle(int id) {
		try {
			Matiere classe = null;
			Optional<MatiereDao> classeDao = servClasseRep.findById(id);
			if(classeDao.isPresent()) {
				classe = matiereDaoToMatiere(classeDao.get());
				servClasseRep.delete(classeDao.get());
			}
			return classe;
		}catch(Exception e) {
		return null;
	}
	}
}
