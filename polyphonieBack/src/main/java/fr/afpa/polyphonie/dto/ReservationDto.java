package fr.afpa.polyphonie.dto;

import java.awt.print.Pageable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.dao.ReservationDao;
import fr.afpa.polyphonie.entite.metier.Matiere;
import fr.afpa.polyphonie.entite.metier.Reservation;
import fr.afpa.polyphonie.entite.metier.Salle;
import fr.afpa.polyphonie.interfaces.dto.IMatiereDto;
import fr.afpa.polyphonie.interfaces.dto.IReservationDto;
import fr.afpa.polyphonie.interfaces.dto.ISalleDto;
import fr.afpa.polyphonie.interfaces.repositories.ReservationRepository;

@Service
public class ReservationDto implements IReservationDto {
	
	@Autowired
	private ReservationRepository reservationRepository;
	
	@Autowired
	private ISalleDto salleDto;
	
	@Autowired
	private IMatiereDto classeDto;
	
	@Override
	public Reservation createReservation(Reservation reservation) {
		ReservationDao reservationDao = reservationToReservationDao(reservation);
		reservationDao.setSalle(SalleDto.salleToSalleDao(reservation.getSalle()));
		reservationDao.setMatiere(MatiereDto.matiereToMatiereDao(reservation.getMatiere()));
		try {
			reservationDao = reservationRepository.save(reservationDao);
			return reservationDaoToReservation(reservationDao);
		}catch (Exception e) {
			Logger.getLogger(ReservationDto.class.getName()).log(Level.SEVERE, null , e);
			return null;
		}
	}

	@Override
	public Reservation getById(int id) {
		Optional<ReservationDao> reservationDao = reservationRepository.findById(id);
		if(reservationDao.isPresent()) {
			Reservation reservation = reservationDaoToReservation(reservationDao.get());
			Salle salle = salleDto.getSalle(reservationDao.get().getSalle().getId_salle());
			Matiere classe = classeDto.getMatiere(reservationDao.get().getMatiere().getId_matiere());
			reservation.setSalle(salle);
			reservation.setMatiere(classe);
			return reservation;
		}
		return null;
	}

	@Override
	public Reservation update(Reservation reservation) {
		ReservationDao reservationDao = reservationToReservationDao(reservation);
		reservationDao.setSalle(SalleDto.salleToSalleDao(reservation.getSalle()));
		reservationDao.setMatiere(MatiereDto.matiereToMatiereDao(reservation.getMatiere()));
		try {
			reservationRepository.saveAndFlush(reservationDao);
			return reservationDaoToReservation(reservationDao);
		}catch (Exception e) {
			Logger.getLogger(ReservationDto.class.getName()).log(Level.SEVERE, null, e);
			return null;
		}
	}

	@Override
	public List<Reservation> getAll(int page, int nbReservPage) {
		//Pageable sortedByDate = PageRequest.of(page, nbReservPage, Sort.by("date_reservation").and(Sort.by("horaire")));
		//Page<ReservationDao> listeReservationDao = reservationRepository.findByDate(LocalDate.now(),
		//		sortedByDate);
		return null;
	}

	@Override
	public int nbPageListeReservation(int nbReservPage) {
		//Pageable sorteByDate = PageRequest.of(0, nbReservPage, Sort.by("date_reservation").and(Sort.by("horaire")));
		return 0;//reservationRepository.findByDate(LocalDate.now(), sorteByDate).getTotalPages();
	}

	@Override
	public Reservation deleteById(int id) {
		try {
			Reservation reservation = null;
			Optional<ReservationDao> reservationDao = reservationRepository.findById(id);
			if(reservationDao.isPresent()) {
				reservation = reservationDaoToReservation(reservationDao.get());
				reservationRepository.delete(reservationDao.get());
			}
			return reservation;
		}catch (Exception e) {
		return null;
			}
		}

	@Override
	public List<Reservation> getAll() {
		List<ReservationDao> listeReservationDao = reservationRepository.findAll(Sort.by("date_reservation").and(Sort.by("horaire")));
		List<Reservation> listeReservation = new ArrayList<Reservation>();
		listeReservationDao.forEach(r -> {
			Reservation reservation = reservationDaoToReservation(r);
			Salle salle = salleDto.getSalle(r.getSalle().getId_salle());
			Matiere classe = classeDto.getMatiere(r.getMatiere().getId_matiere());
			reservation.setSalle(salle);
			reservation.setMatiere(classe);
			listeReservation.add(reservation);
		});
		return listeReservation;
	}
	public static ReservationDao reservationToReservationDao(Reservation reservation) {
		return new ReservationDao(reservation.getId(),reservation.getDate(),reservation.getHoraire(),
				reservation.getDurer());
	}
	public static Reservation reservationDaoToReservation(ReservationDao reservationDao) {
		return new Reservation(reservationDao.getId_reservation(),reservationDao.getDate_reservation(),
				reservationDao.getHoraire(),reservationDao.getDuree());
	}
}
