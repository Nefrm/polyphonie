package fr.afpa.polyphonie.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.dao.AuthentificationDao;
import fr.afpa.polyphonie.entite.dao.UtilisateurDAO;
import fr.afpa.polyphonie.entite.metier.Utilisateur;
import fr.afpa.polyphonie.interfaces.dto.IDtoGeneral;
import fr.afpa.polyphonie.interfaces.dto.IUtilisateurDto;
import fr.afpa.polyphonie.interfaces.repositories.AuthentificationRepository;
import fr.afpa.polyphonie.interfaces.repositories.UtilisateurRepository;

@Service
public class UtilisateurDto implements IUtilisateurDto {

	@Autowired
	private IDtoGeneral dtoGeneral;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private AuthentificationRepository authentificationRepository;

	@Override
	public Map<Integer, Utilisateur> listeUtilisateur() {
		List<UtilisateurDAO> utilisateursDAO = utilisateurRepository.findAll();
		Map<Integer, Utilisateur> utilisateursDTO = new HashMap<Integer, Utilisateur>();
		for (UtilisateurDAO utilisateurDAO : utilisateursDAO) {
			utilisateursDTO.put(utilisateurDAO.getId_utilisateur(),
					dtoGeneral.utilisateurDAOToUtilisateurDTO(utilisateurDAO));
		}
		return utilisateursDTO;
	}

	@Override
	public boolean ajoutUtilisateur(Utilisateur user) {
		UtilisateurDAO userDAO = new UtilisateurDAO();
		userDAO = dtoGeneral.utilisateurToUtilisateurDAO(user);
		AuthentificationDao authDAO = dtoGeneral.authentificationToAuthentificationDAO(user.getAuthentification());
//		authentificationRepository.save(authDAO);
		userDAO.setAuthentification(authDAO);
		System.out.println(userDAO);
		utilisateurRepository.save(userDAO);
		return true;
	}

	@Override
	public Utilisateur listeUtilisateur(String choix) {
		UtilisateurDAO utilisateurDAO = utilisateurRepository.findById(Integer.parseInt(choix)).get();
		return dtoGeneral.utilisateurDAOToUtilisateurDTO(utilisateurDAO);
	}

	@Override
	public boolean supprimerUtilisateur(int choix) {
		utilisateurRepository.deleteById(choix);
		return true;
	}

	@Override
	public boolean updateUtilisateur(Utilisateur user) {
		UtilisateurDAO userDAO = new UtilisateurDAO();
		userDAO = dtoGeneral.utilisateurToUtilisateurDAO(user);
		UtilisateurDAO oldUser = utilisateurRepository.findById(user.getId()).get();
		userDAO.setAuthentification(oldUser.getAuthentification());
		userDAO.setMatiere(oldUser.getMatiere());
		userDAO.setMatieres(oldUser.getMatieres());
		userDAO.setPMatiere(oldUser.getPMatiere());
		userDAO.setMessage(oldUser.getMessage());
		userDAO.setMessages(oldUser.getMessages());
		utilisateurRepository.save(userDAO);
		return true;
	}

}
