package fr.afpa.polyphonie.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.dao.DocumentDAO;
import fr.afpa.polyphonie.entite.metier.Document;
import fr.afpa.polyphonie.entite.transfert.TransfertDocument;
import fr.afpa.polyphonie.interfaces.dto.IDocumentDto;
import fr.afpa.polyphonie.interfaces.dto.IDtoGeneral;
import fr.afpa.polyphonie.interfaces.repositories.DocumentRepository;

@Service
public class DocumentDto implements IDocumentDto {

	@Autowired
	private DocumentRepository documentRepository;
	@Autowired
	private IDtoGeneral dtoGeneral;
	
	@Override
	public Document createDocument(Document document) {
		DocumentDAO documentDao = dtoGeneral.documentToDocumentDAO(document);
		documentRepository.save(documentDao);
		return document;
	}

	@Override
	public Document getDocument(String choix) {
		return dtoGeneral.documentDAOToDocument(documentRepository.findById(Integer.parseInt(choix)).get());
	}

	@Override
	public boolean deleteDocument(String choix) {
		documentRepository.deleteById(Integer.parseInt(choix));
		return true;
	}

	@Override
	public List<Document> getAllDocuments() {
		List<Document> list = new ArrayList<Document>();
		List<DocumentDAO> docsDao = documentRepository.findAll();
		for (DocumentDAO documentDAO : docsDao) {
			list.add(dtoGeneral.documentDAOToDocument(documentDAO));
		}
		return list;
	}

	@Override
	public List<TransfertDocument> listeDocument() {
		List<TransfertDocument> list = new ArrayList<TransfertDocument>();
		List<DocumentDAO> docsDao = documentRepository.findAll();
		for (DocumentDAO documentDAO : docsDao) {
			String[] chemin = documentDAO.getDocument().split("\\\\");
			String nom = chemin[chemin.length-1];
			list.add(new TransfertDocument(nom, ""+documentDAO.getId_document()));
		}
		return list;
	}

	@Override
	public boolean contient(Document document) {
		List<DocumentDAO> docsDao = documentRepository.findAll();
		boolean find = false;
		for (DocumentDAO documentDAO : docsDao) {
			String[] doc = documentDAO.getDocument().split("\\\\");
			if(doc[doc.length-1].equalsIgnoreCase(document.getDocumentUpload().getOriginalFilename())) {
				find = true;
			}
		}
		return find;
	}

}
