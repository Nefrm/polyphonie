package fr.afpa.polyphonie.dto;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import fr.afpa.polyphonie.entite.dao.AuthentificationDao;
import fr.afpa.polyphonie.entite.dao.DocumentDAO;
import fr.afpa.polyphonie.entite.dao.EvenementDao;
import fr.afpa.polyphonie.entite.dao.MessageDAO;
import fr.afpa.polyphonie.entite.dao.RoleDAO;
import fr.afpa.polyphonie.entite.dao.UtilisateurDAO;
import fr.afpa.polyphonie.entite.metier.Authentification;
import fr.afpa.polyphonie.entite.metier.Document;
import fr.afpa.polyphonie.entite.metier.Evenement;
import fr.afpa.polyphonie.entite.metier.Message;
import fr.afpa.polyphonie.entite.metier.Role;
import fr.afpa.polyphonie.entite.metier.Utilisateur;
import fr.afpa.polyphonie.interfaces.dto.IDtoGeneral;
import fr.afpa.polyphonie.interfaces.repositories.RoleRepository;

@Service
public class DtoGeneral implements IDtoGeneral {

	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public Utilisateur utilisateurDAOToUtilisateurDTO(UtilisateurDAO utilisateurDAO) {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setId(utilisateurDAO.getId_utilisateur());
		utilisateur.setNom(utilisateurDAO.getNom());
		utilisateur.setPrenom(utilisateurDAO.getPrenom());
		utilisateur.setMail(utilisateurDAO.getMail());
		utilisateur.setAdresse(utilisateurDAO.getAdresse());
		utilisateur.setDateNaissance(dateToLocalDate(utilisateurDAO.getDateNaissance()));
		utilisateur.setRole(roleDAOToRole(utilisateurDAO.getRole()));
		return utilisateur;
	}

	private Role roleDAOToRole(RoleDAO roleDAO) {
		Role role = new Role();
		role.setId(roleDAO.getId_role());
		role.setLibelle(roleDAO.getLibelle());
		return role;
	}

	private LocalDate dateToLocalDate(Date dateNaissance) {
		return dateNaissance.toLocalDate();
	}

	@Override
	public Authentification authentificationDAOToAuthentification(AuthentificationDao authentificationDao) {
		Authentification authentification = new Authentification();
		authentification.setLogin(authentificationDao.getLogin());
		authentification.setMdp(authentificationDao.getMdp());
		return authentification;
	}

	@Override
	public UtilisateurDAO utilisateurToUtilisateurDAO(Utilisateur user) {
		UtilisateurDAO userDAO = new UtilisateurDAO();
		userDAO.setId_utilisateur(user.getId());
		userDAO.setMail(user.getMail());
		userDAO.setNom(user.getNom());
		userDAO.setPrenom(user.getPrenom());
		userDAO.setAdresse(user.getAdresse());
		userDAO.setDateNaissance(Date.valueOf(user.getDateNaissance()));
		List<RoleDAO> listRole = roleRepository.findByLibelle(user.getRole().getLibelle().toLowerCase());
		RoleDAO roleDAO = new RoleDAO();
		if(!listRole.isEmpty()) {
			roleDAO = listRole.get(0);
		}
		else {
			roleDAO.setLibelle("eleve");
		}
		userDAO.setRole(roleDAO);
		return userDAO;
	}

	@Override
	public AuthentificationDao authentificationToAuthentificationDAO(Authentification authentification) {
		return new AuthentificationDao(authentification.getLogin(), authentification.getMdp());
	}

	@Override
	public EvenementDao mappingEvenementToEvenementDao(Evenement evenement) {
		// TODO Auto-generated method stub
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(evenement, EvenementDao.class);
	}

	@Override
	public Evenement mappingEvenementDaoToEvenement(EvenementDao evenementDao) {
		// TODO Auto-generated method stub
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(evenementDao, Evenement.class);
	}

	@Override
	public Message messageDAOToMessageDTO(MessageDAO messageDAO) {
		
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(messageDAO, Message.class);
	}

	@Override
	public MessageDAO messageToMessageDAO(Message msg) {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(msg, MessageDAO.class);
		
	}

	@Override
	public DocumentDAO documentToDocumentDAO(Document document) {
		DocumentDAO documentDao = new DocumentDAO();
		documentDao.setDocument("src\\main\\resources\\documents\\" + document.getDocumentUpload().getOriginalFilename());
		return documentDao;
	}

	@Override
	public Document documentDAOToDocument(DocumentDAO documentDAO) {
		// TODO Auto-generated method stub
		return null;
	}	

}
