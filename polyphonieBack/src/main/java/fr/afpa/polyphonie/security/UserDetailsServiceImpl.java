package fr.afpa.polyphonie.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.afpa.polyphonie.entite.dao.AuthentificationDao;
import fr.afpa.polyphonie.interfaces.repositories.AuthentificationRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	AuthentificationRepository authRepository;
	
	@Override
	public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
		AuthentificationDao auth = authRepository.findByLogin(username);
		if(auth == null) {
			throw new UsernameNotFoundException("No user named " + username);
		} else {			
			return new UserDetailsImpl(auth);
		}
	}

}
