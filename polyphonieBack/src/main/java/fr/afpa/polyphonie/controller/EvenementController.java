package fr.afpa.polyphonie.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.SerializationFeature;

import fr.afpa.polyphonie.entite.metier.Evenement;
import fr.afpa.polyphonie.interfaces.services.IEvenementMetier;

@CrossOrigin(origins = HomeController.CROSSORIGIN)
@RestController
public class EvenementController {

	@Autowired
	private IEvenementMetier servEvenement;
	
	@PostMapping(value = "/event")
	public Evenement addEvenement(@ModelAttribute Evenement evenement) {
		File fileToSave = null;
		System.out.println("coucou");
		System.out.println(evenement.getImageUploaded().getOriginalFilename());
		
		try {
		/*	fileToSave = new File("C:\\ENV\\Workspace Fayaz\\polyphonie\\polyphonieBack\\images\\"+evenement.getImageUploaded().getOriginalFilename());
			evenement.getImageUploaded().transferTo(fileToSave);
			evenement.setImage("C:\\ENV\\Workspace Fayaz\\polyphonie\\polyphonieBack\\images\\"+evenement.getImageUploaded().getOriginalFilename());
		}catch(IOException ioe) {
			return null;
		}*/
			 	byte[] bytes = evenement.getImageUploaded().getBytes();
	            Path path = Paths.get("src\\main\\resources\\images\\" + evenement.getImageUploaded().getOriginalFilename());
	            Path pathFront = Paths.get("..\\polyphonieFront\\public\\images\\" + evenement.getImageUploaded().getOriginalFilename());
	            Files.write(path, bytes);
	            Files.write(pathFront, bytes);
	            evenement.setImage("src\\main\\resources\\images\\" + evenement.getImageUploaded().getOriginalFilename());
		
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		return servEvenement.createEvenement(evenement);
	}
	
	@GetMapping(value = "/event/{id}")
	public Evenement getEvenement(@PathVariable("id") long id) {
		return servEvenement.getEvenementById(id);
	}
	
	@GetMapping(value = "/events")
	public List<Evenement> getAllEvenement(){
		return servEvenement.getAllEvenement();
	}
	
	@DeleteMapping(value = "/event/{id}")
	public void deleteEvenement(@PathVariable("id") long id){
		servEvenement.deleteEvenement(id);
	}
	
	
	@PutMapping(value = "/updateEvent")
	public Evenement updateEvenement(@ModelAttribute Evenement evenement) {


		System.out.println(evenement.getImageUploaded().getOriginalFilename());
		try {
			
				 	byte[] bytes = evenement.getImageUploaded().getBytes();
		            Path path = Paths.get("src\\main\\resources\\images\\" + evenement.getImageUploaded().getOriginalFilename());
		            Path pathFront = Paths.get("..\\polyphonieFront\\public\\images\\" + evenement.getImageUploaded().getOriginalFilename());
		            Files.write(path, bytes);
		            Files.write(pathFront, bytes);
		            evenement.setImage("src\\main\\resources\\images\\" + evenement.getImageUploaded().getOriginalFilename());
			
			}catch(IOException e) {
				e.printStackTrace();
			}
		return servEvenement.updateEvenement(evenement);
	}
}
