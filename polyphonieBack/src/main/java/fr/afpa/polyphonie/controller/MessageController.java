package fr.afpa.polyphonie.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.polyphonie.entite.metier.Message;
import fr.afpa.polyphonie.entite.metier.Utilisateur;
import fr.afpa.polyphonie.entite.transfert.CreationMessage;
import fr.afpa.polyphonie.interfaces.services.IMessageMetier;

@CrossOrigin(origins = HomeController.CROSSORIGIN)
@RestController
public class MessageController {
	
	@Autowired
	private IMessageMetier iserviceMessage;
	
	
	
	
	@GetMapping(value="/MSGliste")
	@ResponseBody
	public Map<Integer,Message> listerMessage() {
		Map<Integer,Message> listeMessage = iserviceMessage.listerMessage();
	
		return listeMessage;

	}
	
	
	
	@PostMapping(value="/MSGcreation")
	@ResponseBody
	public void creationMessage(@RequestBody CreationMessage message) {
		
		Message msg = new Message();
		
		msg.setTitre(message.getTitre());
		msg.setCorps(message.getCorps());
		msg.setDate(LocalDate.parse(message.getDate()));
		msg.setListeDestinataires(new ArrayList<Utilisateur>());
		iserviceMessage.creerMessage(msg,message.getExpediteur());
		
	}
	
	@DeleteMapping(value = "/MSGdelete")
	@ResponseBody
	public void supprimerMessage(@RequestParam(value = "id_message") String choix) {
		iserviceMessage.supprimerMessage(Integer.parseInt(choix));
	}
}
