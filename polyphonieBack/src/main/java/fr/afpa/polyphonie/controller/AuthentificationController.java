package fr.afpa.polyphonie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.polyphonie.entite.dao.AuthentificationDao;
import fr.afpa.polyphonie.entite.metier.Authentification;
import fr.afpa.polyphonie.interfaces.repositories.AuthentificationRepository;
import fr.afpa.polyphonie.interfaces.services.IAuthentificationMetier;
import fr.afpa.polyphonie.security.UserDetailsImpl;

@CrossOrigin(value = HomeController.CROSSORIGIN)
@RestController
public class AuthentificationController {

	@Autowired
	IAuthentificationMetier authentificationService;
	@Autowired
	AuthentificationRepository ar;
	
	@PostMapping(value = "/SAU")
	@ResponseBody
	public void authentification(@RequestBody Authentification authentification) {
		if(authentificationService.authentification(authentification)) {
			//UserDetailsImpl connectedUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal()
			//AuthentificationDao user = ar.findByLogin(connectedUser.getUsername());
			//System.out.println(user);
			System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		} else {
			System.out.println("troudbal");
		}
	}
}
