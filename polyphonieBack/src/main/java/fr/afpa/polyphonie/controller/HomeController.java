package fr.afpa.polyphonie.controller;

import org.springframework.web.bind.annotation.RestController;

/**
 * Handles requests for the application home page.
 */
@RestController
public class HomeController {
	
	public static final String CROSSORIGIN = "http://localhost:3000";
	
}
