package fr.afpa.polyphonie.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.polyphonie.entite.metier.Matiere;
import fr.afpa.polyphonie.interfaces.services.IMatiereMetier;

@RestController
@CrossOrigin(origins = HomeController.CROSSORIGIN)
public class MatiereController {

	private static final Logger logger = LoggerFactory.getLogger(MatiereController.class);

	@Autowired
	private IMatiereMetier servClasse;

	/**
	 * Methode pour recuperer la liste des salles
	 * 
	 * @param mv     : ModelAndView
	 * @param header
	 * @return le modelAndView crée
	 */
	@GetMapping(value = "/classe")
	public ResponseEntity<?> visualisationClasseGet(ModelAndView mv, @RequestHeader HttpHeaders header) {
		HttpStatus status;
		List<Matiere> listeClasse = servClasse.getAllClasse();
		if (listeClasse != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(listeClasse);
		} else {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return ResponseEntity.status(status).body("Impossible de recupérer les classes");
		}
	}

	/**
	 * Methode pour recuperer une classe via son id
	 * 
	 * @param id     : l'id de la classe
	 * @param header
	 * @return la classe correpondante à l'id
	 */
	@GetMapping(value = "/classe/{id}")
	public ResponseEntity<?> visualisationUneClasseGet(@PathVariable("id") String id,
			@RequestHeader HttpHeaders header) {
		HttpStatus status;
		Matiere classe = servClasse.getClasse(Integer.parseInt(id));
		if (classe != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(classe);
		} else {
			status = HttpStatus.BAD_REQUEST;
			return ResponseEntity.status(status).body("Cette classe n'existe pas !");
		}
	}

	/**
	 * Mehtode qui crée une classe
	 * 
	 * @param classe : la classe crée à sauvegarder
	 * @param header
	 * @return la classe crée à sauvegarder
	 */
	@PostMapping(value = "/classe")
	public ResponseEntity<?> ajouterClassePost(@RequestBody Matiere classe, @RequestHeader HttpHeaders header) {
		HttpStatus status;
		Matiere classeCree = servClasse.createClasse(classe);
		if (classeCree != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(classeCree);
		} else {
			status = HttpStatus.BAD_REQUEST;
			return ResponseEntity.status(status).body("La classe n'a pas pu être crée");
		}
	}

	/**
	 * Methode qui modifie une classe
	 * 
	 * @param classe : la classe à modifier
	 * @param header
	 * @return : la classe modifiée
	 */
	@PutMapping(value = "/classe")
	public ResponseEntity<?> modifierUneClasse(@RequestBody Matiere classe, @RequestHeader HttpHeaders header) {
		HttpStatus status;
		Matiere classeUpdate = servClasse.updateClasse(classe);
		if (classeUpdate != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(classeUpdate);
		} else {
			status = HttpStatus.BAD_REQUEST;
			return ResponseEntity.status(status).body("La classe n'a pas pu être modifiée");
		}
	}

	/**
	 * Methode qui supprime une classe
	 * 
	 * @param id     :l'id de la classe à supprimer
	 * @param header
	 * @return la classe supprimée
	 */
	@DeleteMapping(value = "/classe/{id}")
	public ResponseEntity<?> deleteClasse(@PathVariable("id") Integer id, @RequestHeader HttpHeaders header) {
		HttpStatus status;
		Matiere classeSuppr = servClasse.DeleteClasse(id);
		if (classeSuppr != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(classeSuppr);
		} else {
			status = HttpStatus.BAD_REQUEST;
			return ResponseEntity.status(status).body("La classe n'a pas pu être supprimée");
		}
	}
}
