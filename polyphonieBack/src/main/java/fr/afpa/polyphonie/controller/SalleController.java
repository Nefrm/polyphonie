package fr.afpa.polyphonie.controller;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


import fr.afpa.polyphonie.entite.metier.Salle;
import fr.afpa.polyphonie.interfaces.services.ISalleMetier;

@RestController
@CrossOrigin(origins = HomeController.CROSSORIGIN)
public class SalleController {
	
	private static final Logger logger = LoggerFactory.getLogger(SalleController.class);
	
	@Autowired
	private ISalleMetier servSalle;
	
	/**
	 * Methode pour recuperer la liste des salles
	 * @param mv	: ModelAndView
	 * @param header
	 * @return	le modelAndView crée
	 */
	@GetMapping(value = "/salle")
	public ResponseEntity<?> visualisationsalleGet(ModelAndView mv, @RequestHeader HttpHeaders header){
		HttpStatus status;
		List<Salle> listeSalle =servSalle.getAll();
		if(listeSalle != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(listeSalle);
		}else {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			return ResponseEntity.status(status).body("Impossible de recuperer les salles");
		}
	}
	/**
	 * Methode pour recuperer une salle via son id
	 * @param id	: l'id de la salle de cours	
	 * @param header
	 * @return	la salle de cours correspondante à l'id
	 */
	@GetMapping(value = "/salle/{id}")
	public ResponseEntity<?>visualisationUneSalleGet(@PathVariable("id") String id,
			@RequestHeader HttpHeaders header){
		HttpStatus status;
		Salle salle = servSalle.getSalle(Integer.parseInt(id));
		if(salle != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(salle);
		}else {
			status = HttpStatus.BAD_REQUEST;
			return ResponseEntity.status(status).body("Cette salle n'existe pas !");
		}
	}
	/**
	 * Methode qui crée une salle de cours	
	 * @param salle	:	la salle crée à sauvegarder dans la BDD
	 * @param header
	 * @return	la salle crée à sauvegarder 
	 */
	@PostMapping(value = "/salle")
	public ResponseEntity<?> ajouterSallePost(@RequestBody Salle salle, @RequestHeader HttpHeaders header){
		HttpStatus status;
		Salle salleCree = servSalle.createSalle(salle);
		if(salleCree != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(salleCree);
		}else {
			status = HttpStatus.BAD_REQUEST;
			return ResponseEntity.status(status).body("La salle n'a pas pu être crée");
		}
	}
	/**
	 * Methode qui modifie une salle de cours	
	 * @param salle	: la salle de cours à modifier		
	 * @param header
	 * @return la salle modifiée
	 */
	/*@PutMapping(value = "/salle")
	public ResponseEntity<?> modifierUneSalle(@RequestBody Salle salle, @RequestHeader HttpHeaders header){

	@PutMapping(value = "/salle")
	public ResponseEntity<?> updateSalle(@RequestBody Salle salle, @RequestHeader HttpHeaders header){
 master
		HttpStatus status;
		Salle salleUpdate = servSalle.updateSalle(salle);
		if(salleUpdate != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(salleUpdate);
		}else {
			status = HttpStatus.BAD_REQUEST;
			return ResponseEntity.status(status).body("La salle n'a pa pu être modifiée");
		}
	}*/
	/**
	 * Methode qui supprime une salle 	
	 * @param id	: l'id de la salle à lmodifier
	 * @param header
	 * @return	la salle supprimée
	 */
	@DeleteMapping(value = "/salle/{id}")
	public ResponseEntity<?> deleteSalle(@PathVariable("id") Integer id, @RequestHeader HttpHeaders header){
		HttpStatus status;
		Salle salleSuppr = servSalle.deleteSalle(id);
		if(salleSuppr != null) {
			status = HttpStatus.OK;
			return ResponseEntity.status(status).body(salleSuppr);
		}else {
			status = HttpStatus.BAD_REQUEST;
			return ResponseEntity.status(status).body("La salle n'a pas pu être supprimée");
		}
	}
}
