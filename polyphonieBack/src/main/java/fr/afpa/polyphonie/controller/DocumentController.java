package fr.afpa.polyphonie.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import fr.afpa.polyphonie.entite.metier.Document;
import fr.afpa.polyphonie.entite.transfert.TransfertDocument;
import fr.afpa.polyphonie.interfaces.services.IDocumentMetier;

@CrossOrigin(origins = HomeController.CROSSORIGIN)
@RestController
public class DocumentController {

	@Autowired
	private IDocumentMetier serviceDocument;

	@PostMapping(value = "/SAD")
	public Document ajouterDocument(@ModelAttribute Document document) {
		if(!serviceDocument.contient(document)) {
			try {
				byte[] bytes = document.getDocumentUpload().getBytes();
				Path path = Paths.get("src\\main\\resources\\documents\\" + document.getDocumentUpload().getOriginalFilename());
				Files.write(path, bytes);
			} catch (IOException e) {
				e.printStackTrace();
			}
	
			return serviceDocument.createDocument(document);
		}
		else return null;
	}

	@GetMapping(value = "/STD")
	public Object downloadDocument(@RequestParam(value = "idDoc") String choix) {
		Document rep = serviceDocument.getDocument(choix);
		System.out.println(rep);
		return rep;
	}
	
	@DeleteMapping(value = "/SSD")
	public void deleteDocument(@RequestParam(value = "idDoc") String choix){
		serviceDocument.deleteDocument(choix);
	}

	@GetMapping(value = "/SRD")
	public List<TransfertDocument> getAllDocument(){
		List<TransfertDocument> listeDoc = serviceDocument.listeDocument();
		return listeDoc;
	}
}
