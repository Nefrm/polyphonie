package fr.afpa.polyphonie.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.polyphonie.entite.metier.Authentification;
import fr.afpa.polyphonie.entite.metier.Matiere;
import fr.afpa.polyphonie.entite.metier.Message;
import fr.afpa.polyphonie.entite.metier.Role;
import fr.afpa.polyphonie.entite.metier.Utilisateur;
import fr.afpa.polyphonie.entite.transfert.CreationUtilisateur;
import fr.afpa.polyphonie.entite.transfert.ModificationUtilisateur;
import fr.afpa.polyphonie.entite.transfert.TransfertAuthentification;
import fr.afpa.polyphonie.entite.transfert.TransfertUtilisateur;
import fr.afpa.polyphonie.interfaces.controle.IControleUtilisateur;
import fr.afpa.polyphonie.interfaces.services.IAuthentificationMetier;
import fr.afpa.polyphonie.interfaces.services.IUtilisateurMetier;

@CrossOrigin(origins = HomeController.CROSSORIGIN)
@RestController
//@Secured("admin")
public class UtilisateurController {
	// Controle
	@Autowired
	private IControleUtilisateur controleUtilisateur;
	// Services
	@Autowired
	private IUtilisateurMetier serviceUtilisateur;
	@Autowired
	private IAuthentificationMetier serviceAuthentification;

	@GetMapping(value = "/SRU")
	@ResponseBody
	public TransfertUtilisateur recuperationUtilisateur() {
		TransfertUtilisateur reponse = new TransfertUtilisateur();
		Map<Integer, Utilisateur> listeUtilisateurs = serviceUtilisateur.listeUtilisateur();
		List<Utilisateur> liste = new ArrayList<Utilisateur>();
		for (Entry<Integer, Utilisateur> utilisateur : listeUtilisateurs.entrySet()) {
			liste.add(utilisateur.getValue());
		}
		reponse.setUtilisateur(liste);
		return reponse;

	}

	@GetMapping(value = "/SCURL")
	@ResponseBody
	public TransfertAuthentification recuperationLogin() {
		TransfertAuthentification reponse = new TransfertAuthentification();
		List<Authentification> listeAuthentification = serviceAuthentification.listeAuthentification();
		reponse.setAuthentification(listeAuthentification);
		return reponse;
	}

	@PostMapping(value = "/SCU")
	@ResponseBody
	public void creationUtilisateur(@RequestBody CreationUtilisateur utilisateur) {
		if (controleUtilisateur.verificationInfosUtilisateur(utilisateur)) {
			Utilisateur user = new Utilisateur();
			user.setActif(true);
			user.setClasses(new ArrayList<Matiere>());
			user.setMessages(new ArrayList<Message>());
			user.setRole(new Role(utilisateur.getRole()));
			user.setNom(utilisateur.getNom());
			user.setPrenom(utilisateur.getPrenom());
			user.setMail(utilisateur.getMail());
			user.setAdresse(utilisateur.getAdresse());
			user.setDateNaissance(LocalDate.parse(utilisateur.getDateNaissance()));
			Authentification authentification = new Authentification(utilisateur.getLogin(), utilisateur.getPassword());
			user.setAuthentification(authentification);
			serviceUtilisateur.ajoutUtilisateur(user);
		} else {
			System.out.println("erreur");
		}
	}

	@GetMapping(value = "/SMU")
	@ResponseBody
	public TransfertUtilisateur recuperationUtilisateurModif(@RequestParam(value = "idUser") String choix) {
		TransfertUtilisateur reponse = new TransfertUtilisateur();
		Utilisateur utilisateur = serviceUtilisateur.listeUtilisateur(choix);
		reponse.setUtilisateur(utilisateur);
		return reponse;
	}

	@PutMapping(value = "/SMU")
	@ResponseBody
	public void modificationUtilisateur(@RequestBody ModificationUtilisateur utilisateur) {
		if (controleUtilisateur.verificationInfosUtilisateur(utilisateur)) {
			Utilisateur user = new Utilisateur();
			user.setId(Integer.parseInt(utilisateur.getId()));
			user.setRole(new Role(utilisateur.getRole()));
			user.setNom(utilisateur.getNom());
			user.setPrenom(utilisateur.getPrenom());
			user.setMail(utilisateur.getMail());
			user.setAdresse(utilisateur.getAdresse());
			user.setDateNaissance(LocalDate.parse(utilisateur.getDateNaissance()));
			serviceUtilisateur.updateUtilisateur(user);
		} else {
			System.out.println("erreur");
		}
	}

	@DeleteMapping(value = "/SMU")
	@ResponseBody
	public void supprimerUtilisateur(@RequestParam(value = "idUser") String choix) {
		serviceUtilisateur.supprimerUtilisateur(Integer.parseInt(choix));
	}
}
