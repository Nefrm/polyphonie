package fr.afpa;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import fr.afpa.polyphonie.PolyphoniebackApplication;

@SpringBootTest(classes = PolyphoniebackApplication.class)
class PolyphoniebackApplicationTests {

	@Test
	void contextLoads() {
	}

}
