# Polyphonie

- Groupe :  
	- Dorne Julien
	- Gali Seti
	- Grauwin Guillaume
	- Akbaraly Fayaz

### Livrable ###

	* Diagramme de classe
	* Diagramme de cas d'utilisation
	* Diagramme de séquence
	* Maquette
	* MCD
	* MLD
	* Cahier des charges
	* Script SQL
	* War
	* Code source
	
### Configuration ###

	* Création d'une base de donnée postgres nommé : "polyphonie"  
	* user : admin
	* password : admin
	* Déployer le War
	* Déployer le serveur FRONT
	

### Architecture ###

3 couches :

	- Front
	- Back
	- Base de donnée

### Fonctionnalité ###

	- [x] Créer un utilisateur
	- [x] Lister les utilisateurs
	- [x] Modifier un utilisateur
	- [x] Supprimer un utilisateur
	- [x] Créer un evenement
	- [x] Lister les evenements
	- [x] Modifier un evenement
	- [x] Supprimer un evenement
	- [x] Ajouter un document
	- [x] Lister les documents
	- [x] Supprimer un document
	- [] Messagerie
	- [] Authentification
	- [] gestion de classe

### Technologie ###

BDD :
	- Postgres

Back : 
	- Spring boot
	- Hibernate
	- Spring JPA
	
Front : 
	- React js