import React, { useState, Fragment, useEffect } from 'react';
import Header from '../component/Header';
import Menu from '../component/Menu';
import { Button } from 'react-bootstrap';

export default function ModificationUtilisateur() {
    const [user, setUser] = useState(null);
    const [pret, setPret] = useState(false);
    const [nom, setNom] = useState("");
    const [prenom, setPrenom] = useState("");
    const [email, setEmail] = useState("");
    const [adresse, setAdresse] = useState("");
    const [dateN, setDateN] = useState("");

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handlePrenom = (evt) => {
        setPrenom(evt.target.value);
    }

    const handleEmail = (evt) => {
        setEmail(evt.target.value);
    }

    const handleAdresse = (evt) => {
        setAdresse(evt.target.value);
    }

    const handleDateN = (evt) => {
        setDateN(evt.target.value);
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getUtilisateur(setUser);
            setPret(true);
        }
        fetchJson();
        getUtilisateur(setUser);
    }, []);
    return <Fragment>
        {
            pret ? (
                (
                    <div className="home">
                    <Header />
                    <Menu />
                    <br />
                    <div className="container">
                        <br />
                        <h1 className="text-center">Modification d'un compte</h1>
                        <div className="container"  >
                            <form id="compte" className="form-group">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <label htmlFor="nom">Nom</label>
                                        <input id="nom" className="form-control" type="text" placeholder="ex : Moucron" defaultValue={user.nom} onChange={handleNom} />
                                    </div>
                                    <br />
                                    <div className="col-sm-4">
                                        <label htmlFor="prenom">Prénom</label>
                                        <input id="prenom" className="form-control" type="text" placeholder="ex : Albert" defaultValue={user.prenom} onChange={handlePrenom} />
                                    </div>
                                    <div className="col-sm-4">
                                        <label htmlFor="dateN">Date de naissance</label>
                                        <input id="dateN" className="form-control" type="date" placeholder="ex : 01/04/1998" defaultValue={user.dateNaissance} onChange={handleDateN} />
                                    </div>
                                </div>
                                <br />
                                <div className="row">
                                    <div className="col-sm-4">
                                        <label htmlFor="email">Email</label>
                                        <input id="email" className="form-control" type="mail" placeholder="ex : albert.moucron@gmail.com" defaultValue={user.mail} onChange={handleEmail} />
                                    </div>
                                    <div className="col-sm-8">
                                        <label htmlFor="adresse">Adresse</label>
                                        <input id="adresse" className="form-control" type="text" placeholder="ex : 89 rue du Bidet" defaultValue={user.adresse} onChange={handleAdresse} />
                                    </div>
                                </div>
                                <br />
                                <div className="row">
                                    <div className="col-sm-3">
                                        <label htmlFor="role">Rôle</label><br />
                                        <input name="role" type="radio" value="eleve" />Elève<br />
                                        <input name="role" type="radio" value="professeur" />Professeur
                                </div>
                                </div>
                                <br />
                                <br />
                                <div className="text-center ">
                                    <Button className="btn btn-info mr-5" onClick={() => modifierUtilisateur(user, nom, prenom, email, adresse, dateN)}>valider</Button>
                                </div>
                                <br />
                            </form>
                        </div>
                    </div>
                </div>
                )
            ) : (<div></div>)}
    </Fragment>
}

function modifierUtilisateur(user, nom, prenom, email, adresse, dateN) {
    var r = document.getElementsByName("role");
    var role;
    r.forEach(element => {
        if (element.checked === true) {
            role = element.value;
        }
    });
    
    if (role === undefined) {
        role = user.role.libelle;
    }
    if (nom === "") {
        nom = user.nom
    }
    if (prenom === "") {
        prenom = user.prenom
    }
    if (email === "") {
        email = user.mail
    }
    if (adresse === "") {
        adresse = user.adresse
    }
    if (dateN === "") {
        dateN = user.dateNaissance
    }
    fetch(`${process.env.REACT_APP_API_URL}/SMU`, {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json',
        },
        body: `{
                "id": ${user.id},
                "nom": "${ nom}",
                "prenom": "${ prenom}",
                "mail": "${ email}",
                "adresse": "${ adresse}",
                "dateNaissance": "${ dateN}",
                "role": "${ role}"
            }`
    })
        .catch(error => {
            console.log(error);
        })
}

async function getUtilisateur(setUser) {
    let idUser = document.URL.split("=")[1];
    await fetch(`${process.env.REACT_APP_API_URL}/SMU?idUser=${idUser}`, {
        method: "get",
    })
        .then(response => response.json())
        .then(json => {
            setUser(json.utilisateur);
        })
        .catch(error => console.log(error))
}
