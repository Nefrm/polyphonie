import React from 'react';
import RecupUtilisateurs from '../component/RecupSalles';
import Header from '../component/Header';
import Menu from '../component/Menu';
import { Container } from 'react-bootstrap';
import RecupSalles from '../component/RecupSalles';

export default function ListePersonnes() {
    return (
        <div className="home">
                <Header />
                <Menu />
                <br />
            <Container>
                <br />
                <div>
                    <h1 className="text-center">Liste des Salles</h1>
                </div>
                <br />
                <RecupSalles />
            </Container>
        </div>
    );
}