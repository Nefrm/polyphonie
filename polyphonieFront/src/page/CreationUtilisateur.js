import React, { useState, Fragment } from 'react';
import Header from '../component/Header';
import Menu from '../component/Menu';
import { Button } from 'react-bootstrap';

export default function CreationUtilisateur() {
    const [nom, setNom] = useState("");
    const [prenom, setPrenom] = useState("");
    const [email, setEmail] = useState("");
    const [adresse, setAdresse] = useState("");
    const [role, setRole] = useState("Eleve");
    const [dateN, setDateN] = useState("");

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handlePrenom = (evt) => {
        setPrenom(evt.target.value);
    }

    const handleEmail = (evt) => {
        setEmail(evt.target.value);
    }

    const handleAdresse = (evt) => {
        setAdresse(evt.target.value);
    }

    const handleRole = (evt) => {
        setRole(evt.target.value);
    }

    const handleDateN = (evt) => {
        setDateN(evt.target.value);
    }
    return <Fragment>
        {
            (
                <div className="home">
                <Header />
                <Menu />
                <br />
                <div className="container">
                    <br />
                    <h1 className="text-center">Création d'un compte</h1>
                    <div className="container"  >
                        <form id="compte" className="form-group">
                            <div className="row">
                                <div className="col-sm-4">
                                    <label htmlFor="nom">Nom</label>
                                    <input id="nom" className="form-control" type="text" placeholder="ex : Moucron" onChange={handleNom} />
                                </div>
                                <br />
                                <div className="col-sm-4">
                                    <label htmlFor="prenom">Prénom</label>
                                    <input id="prenom" className="form-control" type="text" placeholder="ex : Albert" onChange={handlePrenom} />
                                </div>
                                <div className="col-sm-4">
                                    <label htmlFor="dateN">Date de naissance</label>
                                    <input id="dateN" className="form-control" type="date" placeholder="ex : 01/04/1998" onChange={handleDateN} />
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-4">
                                    <label htmlFor="email">Email</label>
                                    <input id="email" className="form-control" type="mail" placeholder="ex : albert.moucron@gmail.com" onChange={handleEmail} />
                                </div>
                                <div className="col-sm-8">
                                    <label htmlFor="adresse">Adresse</label>
                                    <input id="adresse" className="form-control" type="text" placeholder="ex : 89 rue du Bidet" onChange={handleAdresse} />
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-3">
                                    <label htmlFor="role">Rôle</label><br />
                                    <input name="role" type="radio" value="ROLE_ELEVE" defaultChecked />Elève<br />
                                    <input name="role" type="radio" value="ROLE_PROFESSEUR" />Professeur
                                </div>
                            </div>
                            <br />
                            <br />
                            <div className="text-center ">
                                <Button className="btn btn-info mr-5" onClick={() => creerUtilisateur(nom, prenom, email, adresse, dateN)}>Créer utilisateur</Button>
                            </div>
                            <br />
                        </form>
                    </div>
                </div>
            </div>
            )}
    </Fragment>
}

function creerUtilisateur(nom, prenom, email, adresse, dateN) {
    var r = document.getElementsByName("role");
    var role;
    r.forEach(element => {
        if (element.checked === true) {
            role = element.value;
        }
    });
    if (controle(nom, prenom, email, adresse, role, dateN)) {
        var login;
        do {
            login = prenom.substr(0, 1) + nom + dateN.substr(2, 2);
            for (let index = 0; index < 3; index++) {
                login += Math.floor(Math.random() * 10);
            }
        } while (verifLogin(login));
        var alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".split("");
        var mdp = nom.substr(0, 1).toUpperCase() + dateN.substr(3, 1);
        for (let index = 0; index < 8; index++) {
            mdp += alpha[Math.floor(Math.random() * 62)];
        }
        fetch(`${process.env.REACT_APP_API_URL}/SCU`, {
            method: "post",
            headers: {
                "Authorization" : `Bearer ${sessionStorage.getItem("token")}`,
                "Content-type": "application/json; charset=UTF-8",
                Accept: 'application/json',
            },
            body: `{
                    "nom": "${ nom}",
                    "prenom": "${ prenom}",
                    "mail": "${ email}",
                    "adresse": "${ adresse}",
                    "role": "${ role}",
                    "dateNaissance": "${ dateN}",
                    "username": "${ login}",
                    "password": "${ mdp}"
                }`
        })
            .catch(error => {
                console.log(error);
            })
    }
    else {
        console.log("non créé");
    }

}

function controle(nom, prenom, email, adresse, role, dateN) {
    if (nom === "") {
        return false;
    }
    if (prenom === "") {
        return false;
    }
    if (email === "") {
        return false;
    }
    if (adresse === "") {
        return false;
    }
    if (dateN === "") {
        return false;
    }
    return true;
}

function verifLogin(login) {
    fetch(`${process.env.REACT_APP_API_URL}/SCURL`, {
        method: "get",
        headers: {
            "Authorization" : `Bearer ${sessionStorage.getItem("token")}`,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }).then(response => response.json())
        .then(json => {
            json.authentification.forEach(auth => {
                if (auth.login === login) {
                    return true;
                }
            });
            return false;
        })
        .catch(error => {
            console.log(error);
        })
}