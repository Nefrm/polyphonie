
import React,{useState} from 'react';
import Header from '../component/Header';

import Footer from '../component/Footer';

import { MDBContainer, MDBJumbotron, MDBIcon, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody } from 'mdbreact';
import MenuStudent from '../component/MenuStudent';


export default function CreationMessage (){

    const [titre, setTitre] = useState("");
    const [corps, setCorps] = useState("");
    
    const [date, setDate] = useState(Date.now);
    const [expediteur, setExpediteur] = useState("");
    const [destinataire, setDestinataire] = useState("");
    
    const handleTitre = (evt) => {
        setTitre(evt.target.value);
    }

    const handleCorps = (evt) => {
        setCorps(evt.target.value);
    }

    const handlDate = (evt) => {
        setDate(evt.target.value);
    }

    const handleExpediteur= (evt) => {
        setExpediteur(evt.target.value);
    }
   
    const handleDestinataire= (evt) => {
        setDestinataire(evt.target.value);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
       
        }
        
        function CreerMessage(titre, corps, date, expediteur, destinataire){
           // var date = new Date().toLocaleDateString('fr', {year: 'numeric',month: 'numeric',day: 'numeric'});
           let today = new Date();
let dd = today.getDate();
let mm = today.getMonth()+1;
const yyyy = today.getFullYear();
if(dd<10)
{
dd=`0${dd}`;
}
if(mm<10)
{
mm=`0${mm}`;
}
today = `${yyyy}-${mm}-${dd}`;
console.log(today);
           
           
           console.log(date)
            var j = 2;
            fetch(`${process.env.REACT_APP_API_URL}/MSGcreation`, {
                method: "post",
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                    Accept: 'application/json',
                },
                body: `{
                        "titre": "${ titre}",
                        "corps": "${ corps}",
                        "date": "${today}",
                        "expediteur": "${ j}",
                        "destinataire": "${ destinataire}"
                        
                        
                    }`
            }).then(function(response){
                console.log(date)
          if(response.status === 200){
              alert('bien envoyé');
              window.location.href = "/Messagerie";
          }
          });
        }
    
     
    return (
        <div className="home">
            <Header />
            <MenuStudent />

            <div>

                <div className="card-deck">



                    <div className="card">
                        <img src={require('../images/ban.png')} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">Polyphonie conservatoire de musique</h5>

                        </div>
                    </div>
                </div>
            </div>


            <div>
                <div className="row">


                    <div className="col-sm-7" id="message">
                        <div className="">
                            <div className="card-body">

                                <form  className="text-center border border-light p-5" action="#!" >

                                    <p className="h4 mb-4">Nouveau message</p>


                                    <br />

                                    <input type="text" id="defaultContactFormName" className="form-control mb-4" placeholder="Destinataire" onChange={handleDestinataire}/>


                                    <input type="texte" id="defaultContactForm" className="form-control mb-4" placeholder="Objet" onChange={handleTitre} />




                                    <div className="form-group">
                                        <textarea className="form-control rounded-0" id="exampleFormControlTextarea2" rows="3" placeholder="Message"  onChange={handleCorps}></textarea>
                                    </div>

                                    <button type="button" className="send" onClick={() => CreerMessage(titre, corps, date, expediteur, destinataire)}>Envoyer</button> <br />

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <Footer />
            </div>
        </div>
    );

    }