import React from 'react';
import Header from '../component/Header';
import Menu from '../component/Menu';
import RecupEvenements from '../component/RecupEvenements';
import { Container } from 'react-bootstrap';

export default function AfficherEvenement() {
    return (
        <div className="home">
            <Header />
            <Menu />
            <br />
            <Container>
                <br />
                <div>
                    <h1 className="text-center">Evènement du Jeudi soir</h1>                 
                </div>
                <RecupEvenements />
                <br />
            </Container>
        </div>
    );
}