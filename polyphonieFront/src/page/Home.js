import React from 'react';
import Car from '../component/Car';
import {
  MDBContainer, MDBRow, MDBCol, MDBJumbotron, MDBBtn
} from "mdbreact";
import Footer from '../component/Footer';
import Header from '../component/Header';
import Menu from '../component/Menu';





export default function Home() {
  return (
    <div className="home">
      <Header />
      <Menu />
      <div>   <Car />   </div>
      <div>

        <div className="card-deck">
          <div className="card">
            <img src={require('../images/violon.jpg')} className="card-img-top" alt="..." />
            <div className="card-body">
              <h5 className="card-title">Nos cours</h5>

            </div>
          </div>
          <div className="card">
            <img src={require('../images/harpe.jpg')} className="card-img-top" alt="..." />
            <div className="card-body">
              <h5 className="card-title">Nos instruments</h5>

            </div>
          </div>
          <div className="card">
            <img src={require('../images/micro.jpg')} className="card-img-top" alt="..." />
            <div className="card-body">
              <h5 className="card-title">Nos salles</h5>

            </div>
          </div>
        </div>
      </div>
      <div className="presentation" >
        <MDBContainer className="mt-5 text-center">
          <MDBRow>
            <MDBCol>
              <MDBJumbotron>
                <h2 className="h1 display-3">Appellez-nous pour un cours d'essai</h2>
                <p className="lead">
                  Situé au coeur du quartier de la Madeleine, le conservatoire polyphonie vous acceuille pour vous faire découvrir et vous former à la musique
            </p>
                <hr className="my-2" />
                <p>
                  Si vous avez des questions n'hésitez pas à nous contacter
            </p>
                <p className="lead">
                  <MDBBtn color="dark">Nous contacter</MDBBtn>
                </p>
              </MDBJumbotron>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}
