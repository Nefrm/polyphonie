import React, { useState, Fragment } from "react";
import Header from '../component/Header';
import Menu from '../component/Menu';
import { MDBContainer, MDBRow, MDBCol, MDBInput } from 'mdbreact';

export default function CreationEvenement() {

  const [titre, setTitre] = useState('');
  const [contenu, setContenu] = useState('');
  const [image, setImage] = useState([]);


  const changeHandlerTitre = e => {
    setTitre(e.target.value);
  };
  const changeHandlerContenu = e => {
    setContenu(e.target.value);
  };

  const changeHandlerImage = e => {
    setImage(e.target.files[0]);
  };

  function handleSubmitForm(e) {
    e.preventDefault();

    const formData = new FormData();

    formData.append("titre", titre);
    formData.append("contenu", contenu);
    formData.append("imageUploaded", image);



    fetch(`${process.env.REACT_APP_API_URL}/evenements`, {
      method: 'POST',
      mode: 'no-cors',
      headers: {
        "Content-type": false
      },
      body: formData
    }).then(function (response) {
      //status 200 veut dire que tout s'est bien passé sur Postman

      window.location.href = "/Evenement";

    });


  }

  return <Fragment>
    <div className="home">
            <Header />
            <Menu />
            <br />
    <MDBContainer>
    
           
      <MDBRow>
        <MDBCol md="6">
          <form encType="multipart/form-data">
            <p className="h5 text-center mb-4">Créer un évènement</p>
            <div className="grey-text">
              <MDBInput label="Entrez un titre" type="text" validate error="wrong" success="right" id="titre" name="titre" value={titre} onChange={changeHandlerTitre} required />
              <MDBInput label="Entrez votre programme" type="textarea" validate error="wrong" success="right" id="contenu" name="contenu" value={contenu} onChange={changeHandlerContenu} required />
            </div>
            
            <div className="form-group">
              <label htmlFor="file">Envoyer une image </label>
              <input type="file" className="form-control-file" id="file" name="imageUploaded" onChange={changeHandlerImage} />
            </div>
           
            <div className="text-center">
              <button type="submit" className="btn btn-primary" onClick={handleSubmitForm}>Valider</button>
            </div>
          </form>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
</div>
  </Fragment>;
} 