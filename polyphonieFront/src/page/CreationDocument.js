import React, { useState, Fragment } from "react";
import Menu from '../component/Menu';
import Header from "../component/Header";
import { Container } from "react-bootstrap";

export default function CreationDocument() {
  const [document, setDocument] = useState([]);

  const changeHandlerDocument = e => {
    setDocument(e.target.files[0]);
  };

  function handleSubmitForm(e) {
    e.preventDefault();

    const formData = new FormData();
    formData.append("documentUpload", document);

    fetch(`${process.env.REACT_APP_API_URL}/SAD`, {
      method: 'POST',
      mode: 'no-cors',
      headers: {
        "Content-type": false
    },
      body: formData
    }).then(function (response) {
      //status 200 veut dire que tout s'est bien passé sur Postman
     
        window.location.href = "/ajouterdocument";
    
    });


  }

  return <Fragment>
    <div className="home">
                <Header />
                <Menu />
                <br />
            <Container>
      <form encType="multipart/form-data">

        <div className="form-group col-md-6">
          <label htmlFor="file">Insérer un document :</label>
          <input type="file" id="file" name="documentUploaded" onChange={changeHandlerDocument} />
        </div>

        <button type="submit" className="btn btn-primary" onClick={handleSubmitForm}>Valider</button>
      </form>
      </Container>
    </div>

  </Fragment>;
} 