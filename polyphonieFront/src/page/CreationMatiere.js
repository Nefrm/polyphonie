import React, { useState, Fragment } from "react";
import Header from '../component/Header';
import Menu from '../component/Menu';
import { MDBContainer, MDBRow, MDBCol, MDBInput } from 'mdbreact';

export default function CreationMatiere() {

  const [libelle, setLibelle] = useState('');



  const changeHandlerLibelle = e => {
    setLibelle(e.target.value);
  };


  function handleSubmitForm(e) {
    e.preventDefault();

    const formData = new FormData();

    formData.append("libelle", libelle);
    



    fetch(`${process.env.REACT_APP_API_URL}/matieres`, {
      method: 'POST',
      mode: 'no-cors',
      headers: {
        "Content-type": false
      },
      body: formData
    }).then(function (response) {
      //status 200 veut dire que tout s'est bien passé sur Postman

      window.location.href = "/Matiere";

    });


  }

  return <Fragment>
    <div className="home">
            <Header />
            <Menu />
            <br />
    <MDBContainer>
    
           
      <MDBRow>
        <MDBCol md="6">
          <form encType="multipart/form-data">
            <p className="h5 text-center mb-4">Créer une matière</p>
            <div className="grey-text">
              <MDBInput label="Entrez le nom de la matière" type="text" validate error="wrong" success="right" id="libelle" name="libelle" value={libelle} onChange={changeHandlerLibelle} required />
            </div>
            
           
           
            <div className="text-center">
              <button type="submit" className="btn btn-primary" onClick={handleSubmitForm}>Valider</button>
            </div>
          </form>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
</div>
  </Fragment>;
} 