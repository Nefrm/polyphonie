import React from 'react';
import Header from '../component/Header';
import Menu from '../component/Menu';
import Footer from '../component/Footer';

import { MDBContainer, MDBJumbotron, MDBIcon, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody } from 'mdbreact';


export default function Contact() {

  return (
    <div class="home">
      <Header />
      <Menu />

      <div>

        <div class="card-deck">



          <div class="card">
            <img src={require('../images/ban.png')} class="card-img-top" alt="..." />
            <div class="card-body">
              <h5 class="card-title">Polyphonie conservatoire de musique</h5>

            </div>
          </div>
        </div>
      </div>


      <div>
        <div class="row">
          <div class="col-sm-6 mb-3 mb-md-0">
            <div class="card">
              <div class="card-body">
                <div className="pres" >

                  <div class="jumbotron text-center hoverable p-4">


                    <div class="row">


                      <div class="col-md-4 offset-md-1 mx-3 my-3">

                        <div class="view overlay">
                          <img src={require('../images/violon.jpg')} class="img-fluid" alt="Sample image for first version of blog listing" />
                          <a>
                            <div class="mask rgba-white-slight"></div>
                          </a>
                        </div>

                      </div>

                      <div class="col-md-7 text-md-left ml-3 mt-3">




                        <h4 class="h4 mb-4">Vous pouvez nous joindre du lundi au samedi</h4>

                        <p class="font-weight-bold">Le standard : 01234567</p>
                        <p class="font-weight-bold">Le sécretariat : 01234567</p>
                        <p class="font-weight-bold">La direction : 01234567</p>

                      </div>


                    </div>


                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="card">
              <div class="card-body">

                <form class="text-center border border-light p-5" action="#!">

                  <p class="h4 mb-4">Nous envoyer un message</p>


                  <input type="text" id="defaultContactFormName" class="form-control mb-4" placeholder="Nom" />


                  <input type="email" id="defaultContactFormEmail" class="form-control mb-4" placeholder="E-mail" />




                  <div class="form-group">
                    <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3" placeholder="Message"></textarea>
                  </div>



                  <button type="button" className="send">Envoyer</button>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <Footer />
      </div>
    </div>
  );
}
