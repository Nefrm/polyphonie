import React from 'react';
import RecupDocuments from '../component/RecupDocuments';
import Header from '../component/Header';
import Menu from '../component/Menu';
import { Container } from 'react-bootstrap';

export default function ListerDocument() {
    return (
        <div className="home">
                <Header />
                <Menu />
                <br />
            <Container>
                <br />
                <div>
                    <h1 className="text-center">Liste des Documents</h1>
                </div>
                <br />
                <RecupDocuments />
            </Container>
        </div>
    );
}