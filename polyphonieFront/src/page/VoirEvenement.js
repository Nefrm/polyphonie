import React, { useState, useEffect, Fragment } from "react";
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import { useParams } from 'react-router-dom';
import Header from '../component/Header';
import Menu from '../component/Menu';

export default function VoirEvenement() {

    const [idEvenement, setIdEvenement] = useState('');
    const [evenement, setEvenement] = useState([]);
    const [titre, setTitre] = useState('');
    const [contenu, setContenu] = useState('');
    const [image, setImage] = useState('');
    const [pret, setPret] = useState([]);
    const [modal, setModal] = useState(false);
    let { id } = useParams();



    const toggle = () => {
        setModal(!modal);
    }

  

    function changeUrl(string){
        const strTab  = string.split('\\');
        let i = strTab.length;
        const str = strTab[i-1];
        return str;
    }


    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setEvenement);
            setPret(true);
        }
        fetchJson();
        recup(setEvenement);
    }, []);

    while (evenement === []) {
        recup(setEvenement);
    }



   

    async function recup(setEvenement) {
        await fetch(`${process.env.REACT_APP_API_URL}/event/${id}`, {
            method: "get",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })
            .then(response => { return response.json() })
            .then(data => {
                setEvenement(data);
                setIdEvenement(data.id_evenement);
                setTitre(data.titre);
                setContenu(data.contenu);
                setImage(changeUrl(data.image));
                
                
            })
            .catch(error => { console.log(error); })
    }

    return <Fragment>
        {
            pret ? (
                
                <div className="home">
                <Header />
                <Menu />
                <br />
                    <MDBContainer>
                        <MDBBtn onClick={toggle}>Voir</MDBBtn>
                        <MDBModal isOpen={modal} toggle={toggle} size="medium">
                            <MDBModalHeader toggle={toggle} ><img src={`/images/${image}`} width="450px"></img></MDBModalHeader>
                            <MDBModalBody>
                                <p>{titre}</p>
                                <hr />
                                <p>{contenu}</p>
                            </MDBModalBody>
                            <MDBModalFooter>
                                <MDBBtn color="secondary" onClick={toggle}>Femer</MDBBtn>
                            </MDBModalFooter>
                        </MDBModal>
                    </MDBContainer>
                </div>
            ) : (<div></div>)}


    </Fragment>;

}
