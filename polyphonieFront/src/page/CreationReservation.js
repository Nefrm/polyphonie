import React, { useState, useEffect, Fragment } from "react";
import Header from '../component/Header';
import Menu from '../component/Menu';
import { MDBContainer, MDBRow, MDBCol, MDBInput } from 'mdbreact';

export default function CreationReservation() {

    const [matieres, setMatieres] = useState([]);
    const [salles, setSalles] = useState([]);
    const [reservation, setReservation] = useState('');
    const [dateDebutR, setDateDebutR] = useState([]);
    const [dateFinR, setDateFinR] = useState([]);
    const [idMatiere, setIdMatiere] = useState(0);
    const [idSalleR, setIdSalleR] = useState(0);
    const [isBusy, setBusy] = useState(true);
  

    const getMatieres = () => {
        fetch(`${process.env.REACT_APP_API_URL}/matieres`, {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })
          .then(response => {
            return response.json();
          })
          .then(data => {
            setMatieres(data);
            setIdMatiere(data[0].id);
            
            setBusy(false);
          })
          .catch(error => {
            console.log(error);
          });
      }
  
    
    const getSalles = () => {
      fetch(`${process.env.REACT_APP_API_URL}/salles`, {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      })
        .then(response => {
          return response.json();
        })
        .then(data => {
          setSalles(data);
          setIdSalleR(data[0].id);
          setBusy(false);
        })
        .catch(error => {
          console.log(error);
        });
    }
  
    useEffect(() => {
      const fetchData = async () => {
        await getMatieres();
        await getSalles();
      }
      fetchData()
    }, []);
  
    const changeHandlerReservation = e => {
      setReservation(e.target.value);
    };
  
    // à chaque modification du champ, il set les données du champ
    const changeHandlerDateDebut = e => {
      setDateDebutR(e.target.value);
    };
    const changeHandlerDateFin = e => {
      setDateFinR(e.target.value);
    };
  
    const changeHandlerIDMatiere = e => {
      setIdMatiere(e.target.value);
  
    };
  
    const changeHandlerIDSalle = e => {
      setIdSalleR(e.target.value);
  
    };
  
  
    function handleSubmit(e) {
      //il permet de ne pas recharger la page avant d'envoyer les données
      e.preventDefault();
  
      if(compare()){
          //pour envoyer les données 
          console.log(dateDebutR);
          console.log(idMatiere);
          console.log(idSalleR);
      fetch(`${process.env.REACT_APP_API_URL}/reservations`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json; charset=UTF-8'
        },
        // .stringgify pour transformer en string
        body: JSON.stringify({
          date: dateDebutR,
          duree: 50,
          matiereId : idMatiere,     
          salleId : idSalleR
          
        })
      }).then(function (response) {
        //status 200 veut dire que tout s'est bien passé sur Postman
        if (response.status === 200) {
         alert('ok, bien envoyé');
          window.location.href = "/RecupReservations";
        }
      });
      }else{
        
        alert('pas ok');
      }
      
      
    }
    return <Fragment>
    <div className="home">
    <Header />
    <Menu />
    <br />
    <MDBContainer>
  
      {isBusy ? (<div></div>) : (
        <div>
          <form >
  
            <div className="form-group col-md-6">
              <label htmlFor="nom">Nom de la reservation</label>
              <input type="text" className="form-control" id="nom" placeholder="Nom de la reservation" name="nom" value={reservation} onChange={changeHandlerReservation} required />
            </div>
  
  
            <div className="form-group col-md-6">
              <label htmlFor="startDate">Date de début :</label>
              <input type="date" className="form-control" id="startDate" name="dateReservationDebut" value={dateDebutR} onChange={changeHandlerDateDebut} required />
            </div>
  
            <div className="form-group col-md-6">
              <label htmlFor="endDate">Date de fin :</label>
              <input type="date" className="form-control" id="endDate" name="dateReservationFin" value={dateFinR} onChange={changeHandlerDateFin} required />
            </div>
  
            <p> pour quel prof ?</p>
            
            <div className="form-group col-md-6">
              <label htmlFor="selectPersonne">Sélectionner la matière  :</label> 
              <select className="form-control" id="selectMatiere" name="selectMatiere" value={idMatiere} onChange={changeHandlerIDMatiere}>
              {matieres.map((matiere) =>
             
                <option key = {matiere.id} value={matiere.id}>{matiere.libelle}</option>
                )}
              </select>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="selectSalle">Quelle salle voulez-vous réserver ? </label> 
              <select className="form-control" id="selectSalle" name="selectSalle" value={idSalleR} onChange={changeHandlerIDSalle}>
              {salles.map((salle) =>
             
                <option key = {salle.id} value={salle.id}>{salle.nom}</option>
                )}
              </select>
            </div>
  
            <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Valider</button>
          </form>
        </div>
      )}
      </MDBContainer>
      </div>
    </Fragment>;
  
  function compare()
  {
      var startDt = document.getElementById("startDate").value;
      var endDt = document.getElementById("endDate").value;
      if( (new Date(startDt).getTime() < new Date(endDt).getTime()))
      {
          return true;
      }
    return false;
  }
  }
  
  