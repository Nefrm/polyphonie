import React, { useState } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';
import Header from '../component/Header';
import Menu from '../component/Menu';

export default function FormPage (){

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [isBusy, setBusy] = useState(true);
    
  
    const changeHandlerLogin = e => {
      setLogin(e.target.value);
    };
  
    const changeHandlerPassword = e => {
      setPassword(e.target.value);
  
    };
    
  
  return (
    <div className="home">
    <Header />
    <Menu />
    <br />
  <MDBContainer className="formLogin">
    <MDBRow>
      <MDBCol md="6" className="formConnexion">
        <form>
          <div className="logoFormConnect">
              <MDBRow>
                  <MDBCol>
                      <img src={require('../images/violon.jpg')} id="logo" className="img-fluid rounded-circle" alt="logo polyphonie" />
                  </MDBCol>
              </MDBRow>
          </div>
          <p className="h5 text-center mb-4">CONNEXION</p>
          <div className="grey-text">
            <MDBInput label="Entrez votre login" icon="user" group type="text" containerClass="text-left" validate error="wrong" success="right" name="login" value={login} onChange={changeHandlerLogin} />
            <MDBInput label="Entrez votre mot de passe" icon="lock" group type="password" containerClass="text-left" name = "password" value={password} onChange={changeHandlerPassword} validate />
          </div>
          <div className="text-center">
            <MDBBtn onClick={()=> {submit(login, password)}}>Se connecter</MDBBtn>
          </div>
        </form>
      </MDBCol>
    </MDBRow>
  </MDBContainer>
  </div>
  );
  };
  
function submit(login, password){
  fetch(`${process.env.REACT_APP_API_URL}/authenticate`, {
    method: "post",
    headers: {
        'Content-type': "application/json",
        'Accept': 'application/json',
    },
    body: JSON.stringify({
            username: login,
            password: password
        })
}) 
    .then((response) => {
      response.json()
        .then((json) => {
          sessionStorage.setItem("token", json.id_token)
        })
      })
      .catch(error => {
        console.log(error);
      })
      if(sessionStorage.getItem("token") != undefined) {
        fetch(`${process.env.REACT_APP_API_URL}/account`, {
          method: "get",
          headers: {
            "Authorization" : `Bearer ${sessionStorage.getItem("token")}`
          }
        }).then((response) => {
          response.json()
          .then((json) => {
            console.log(json)
            sessionStorage.setItem("authorities", json.authorities)
            //document.location.href = "/"
          })
        }).catch(error => {
          console.log(error);
        })
      }
  }