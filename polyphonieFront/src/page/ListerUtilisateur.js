import React from 'react';
import RecupUtilisateurs from '../component/RecupUtilisateurs';
import Header from '../component/Header';
import Menu from '../component/Menu';
import { Container } from 'react-bootstrap';

export default function ListePersonnes() {
    return (
        <div className="home">
                <Header />
                <Menu />
                <br />
            <Container>
                <br />
                <div>
                    <h1 className="text-center">Liste des Utilisateurs</h1>
                </div>
                <br />
                <RecupUtilisateurs />
            </Container>
        </div>
    );
}