import React, { useState, useEffect, Fragment } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput } from 'mdbreact';
import Header from '../component/Header';
import Menu from '../component/Menu';
import { useParams } from 'react-router-dom';

export default function CreationEvenement() {
    const [idEvenement, setIdEvenement] = useState('');
    const [evenement, setEvenement] = useState([]);
    const [titre, setTitre] = useState('');
    const [contenu, setContenu] = useState('');
    const [image, setImage] = useState([]);
    const [pret, setPret] = useState([]);

    let { id } = useParams();

    const changeHandlerTitre = e => {
        setTitre(e.target.value);
    };
    const changeHandlerContenu = e => {
        setContenu(e.target.value);
    };

    const changeHandlerImage = e => {
        setImage(e.target.files[0]);
    };

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setEvenement);
            setPret(true);
        }
        fetchJson();
        recup(setEvenement);
    }, []);

    while (evenement === []) {
        recup(setEvenement);
    }

    async function recup(setEvenement) {
        await fetch(`${process.env.REACT_APP_API_URL}/event/${id}`, {
            method: "get",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })
            .then(response => { return response.json() })
            .then(data => {
                setEvenement(data);
                setIdEvenement(data.id_evenement);
                setTitre(data.titre);
                setContenu(data.contenu);
            })
            .catch(error => { console.log(error); })
    }

    function handleSubmitForm(e) {
        e.preventDefault();

        const formData = new FormData();

        formData.append("id_evenement", idEvenement);
        formData.append("titre", titre);
        formData.append("contenu", contenu);
        formData.append("imageUploaded", image);
    


        fetch(`${process.env.REACT_APP_API_URL}/updateEvent`, {
            method: 'put',
            headers: {
                
            },
            body: formData
        }).then(function (response) {
            //status 200 veut dire que tout s'est bien passé sur Postman
        });


    }

    return <Fragment>
        {
            pret ? (
                <div className="home">
                <Header />
                <Menu />
                <br />
                <MDBContainer>
                    <MDBRow>
                        <MDBCol md="6">
                            <form encType="multipart/form-data">
                                <p className="h5 text-center mb-4">Modifier un évènement</p>
                                <div className="grey-text">
                                    <input type="hidden" name="id_evenement" value={idEvenement} />
                                    <MDBInput label="Entrez un titre" type="text" validate error="wrong" success="right" id="titre" name="titre" value={titre} onChange={changeHandlerTitre} required />
                                    <MDBInput label="Entrez votre programme" type="textarea" validate error="wrong" success="right" id="contenu" name="contenu" value={contenu} onChange={changeHandlerContenu} required />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="file">Envoyer une image </label>
                                    <input type="file" className="form-control-file" id="file" name="imageUploaded" onChange={changeHandlerImage} />
                                </div>

                                <div className="text-center">
                                    <button type="submit" className="btn btn-primary" onClick={handleSubmitForm}>Valider</button>
                                </div>
                            </form>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                </div>
            ) : (<div></div>)}


    </Fragment>;
} 