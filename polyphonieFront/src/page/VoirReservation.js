import React, { useState, useEffect, Fragment } from 'react';
import { useParams, Link } from 'react-router-dom';


export default function VoirReservation() {
  const [reservation, setReservation] = useState({});
  const [isBusy, setBusy] = useState(true);
  let { id } = useParams();


  const getReservation = () => {
    
    fetch(`${process.env.REACT_APP_URL}/reservations/${id}`, {
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
    })
    .then(response => {
      return response.json();
    })
    .then(data => {
      
      setReservation(data);
      setBusy(false);
    })
    .catch(error => {
      console.log(error);
    });
  }

  useEffect(() => {
    const fetchData = async () => {
      
        await getReservation();
        
    }
    fetchData()
    
  }, []);

  const dateFormatMois = (mois) => {
      if(mois < 10){
        mois = "0"+mois;
      }
      return mois;
  }
  

  return <Fragment>
        <div className="container" key={reservation.id}>
        { isBusy ? (<div>coucou</div>) : (
          <div>
            <p>Description de la reservation : { reservation.description } </p>
            <p>La salle : { reservation.salle.nom } { reservation.personne.prenom }est réservé le {dateFormatMois(reservation.dateDebut[2])}/{dateFormatMois(reservation.dateDebut[1])}/{reservation.dateDebut[0]} au {dateFormatMois(reservation.dateFin[2])}/{dateFormatMois(reservation.dateFin[1])}/{ reservation.dateFin[0]}</p>          
              <Link to={`/ModificationReservation/${reservation.id}`}><button className="btn btn-primary" type="submit">Modifier</button></Link>
        </div>
        )}
      </div>
      </Fragment>

  
    }

