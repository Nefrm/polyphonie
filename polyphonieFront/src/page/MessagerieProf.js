import React from 'react';
import Header from '../component/Header';

import Footer from '../component/Footer';

import { MDBContainer, MDBJumbotron, MDBIcon, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody } from 'mdbreact';
import MenuStudent from '../component/MenuStudent';
import MenuProf from '../component/MenuProf';


export default class MessagerieProf extends React.Component {



  
render(){
  return (
    <div class="home">
      <Header />
      <MenuProf />

      <div>

        <div class="card-deck">



          <div class="card">
            <img src={require('../images/ban.png')} class="card-img-top" alt="..." />
            <div class="card-body">
              <h5 class="card-title">Polyphonie conservatoire de musique</h5>

            </div>
          </div>
        </div>
      </div>


      <div>
        <div class="row">
          <div class="col-sm-3 mb-3 mb-md-0">
            <div class="card">
              <div class="card-body">
                <div className="pres" >

                  <table class="table">
                    <thead class="black white-text">
                      <tr>

                        <th scope="col">De :</th>
                        <th scope="col">Objet :</th>

                      </tr>
                    </thead>
                    <tbody>
                      <tr>

                        <td>Admin</td>
                        <td>Meeting</td>

                      </tr>


                    </tbody>
                  </table>



                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-7">
            <div class="card">
              <div class="card-body">

                <form class="text-center border border-light p-5" action="#!">

                  <p class="h4 mb-4">Messagerie</p>
                 <a href="/Message"> <button type="button" className="send" >Nouveau message</button></a>
                  <button type="button" className="send">Repondre</button> <br />
                  <br />

                  <input type="text" id="defaultContactFormName" class="form-control mb-4" placeholder="Expediteur" />


                  <input type="texte" id="defaultContactFormEmail" class="form-control mb-4" placeholder="Objet" />




                  <div class="form-group">
                    <textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3" placeholder="Message"></textarea>
                  </div>



                </form>

              </div>
            </div>
          </div>
        </div>
      </div>

      <div>
        <Footer />
      </div>
    </div>
  );
}
}
