import React from "react";

import Chemin from './chemin/Chemin';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <Chemin />
    </div>
  );
}

export default App;

