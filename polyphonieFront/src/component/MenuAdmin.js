import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

class MenuAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
            url: ''
        };
        this.onClick = this.onClick.bind(this);

    }

    onClick() {
        this.setState({
            collapse: !this.state.collapse,
        });
    }



    render() {
        return (

            <nav className="navbar navbar-expand-lg navbar-light bg-dark">
                <a className="navbar-brand" href="Home">Polyphonie</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="#">Informations <span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/Contact">Contact</a>
                        </li>
                        <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Gestion
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <a className="dropdown-item" href="#">Comptes</a>
          <a className="dropdown-item" href="#">Documents</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#">Evenements</a>
        </div>
      </li>
      <li className="nav-item">
                            <a className="nav-link" href="/Messagerie">Messagerie</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Compte</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Deconnexion</a>
                        </li>
                    </ul>
                   
                </div>
            </nav>

        )
    }
}

export default MenuAdmin