import React, { useState, useEffect, Fragment } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function RecupUtilisateurs() {
    const [pret, setPret] = useState([]);
    const [liste, setListe] = useState([]);

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListe);
            setPret(true);
        }
        fetchJson();
        recup(setListe);
    }, []);
    
    while (liste == []){
        recup(setListe);
    }
    return <Fragment>
        {
            pret ? (
                <div className="Container">
                    <div className="row col-md-12 col-md-offset-2 ">
                        <table className=" table table-striped fond">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Nom</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            {recupListe(liste)}
                        </table>
                    </div>
                </div>
            ) : (<div></div>)}
    </Fragment>
}

function recupListe(liste) {
    return (
        <tbody>
            {liste.map((document) =>
                <tr key={document.id}>
                    <td>{document.nom}</td>
                    <td><Button onClick={() => downloadDocument(document.id)} className="btn btn-primary btn-block">télécharger</Button></td>
                    <td><Button onClick={() => supprimerDocument(document.id)} className="btn btn-danger btn-block">supprimer</Button></td>
                </tr>
            )}
        </tbody>
    );
}

async function recup(setListe) {
    await fetch(`${process.env.REACT_APP_API_URL}/SRD`, {
        method: "get",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
        .then(response => response.json())
        .then(json => {
            setListe(json);
        })
        .catch(error => { console.log(error); })
}

async function supprimerDocument(idDoc) {
    await fetch(`${process.env.REACT_APP_API_URL}/SSD?idDoc=${idDoc}`, {
        method: "delete",
    }).then(() => {
        document.location.reload();
    })
        .catch(error => {
            console.log(error);
        })
}

async function downloadDocument(idDoc) {
    console.log(idDoc);
    /*await fetch(`${process.env.REACT_APP_API_URL}/STD?idDoc=${idDoc}`, {
        method: "delete",
    }).then(() => {
        document.location.reload();
    })
        .catch(error => {
            console.log(error);
        })*/
}