import React, { useState, Fragment } from "react";
import Header from '../component/Header';
import Menu from '../component/Menu';
import { MDBContainer, MDBRow, MDBCol, MDBInput } from 'mdbreact';

import { Link } from 'react-router-dom';

export default function RecupReservations() {

    const [reservations, setReservations] = useState([]);

    const getReservations = () => {


        //lien à modifier
        fetch(`${process.env.REACT_APP_URL}/reservations`, {
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
             })

            .then(res => {
                return res.json();
            })

            .then(data => {

                setReservations(data);
                console.log(reservations)
            })

            .catch(error => {
                console.log(error);

            })
    }
    useEffect(() => {
        
        const fetchData = async () => {
      
            await getReservations();
        }
        fetchData()
    }, []);

    const reservationsList = reservations.map((reservation) =>
    
        <div className="col-sm-4" key={reservation.id}>
            <div className="card" >
            <img className="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="une salle" />
                <div className="card-body">
                    <h5 className="card-title">{ reservation.nom }</h5>
                    <p className="card-text">La salle { reservation.salle.nom } a été reservé</p>
                    <Link to={`/voirReservation/${reservation.id}`}><button className="btn btn-primary" type="submit">Voir le détail</button></Link>
                    <input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteReservation(reservation.id)}} />
                </div>
            </div>

        </div>);
    
    return <div className="bg">
           <div className="container">
           <div className="row">
                {reservationsList}
                </div>
           </div>      
        </div>


function deleteReservation(id) {
    fetch(`${process.env.REACT_APP_URL}/reservation/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/RecupReservations";

}

}

  
  