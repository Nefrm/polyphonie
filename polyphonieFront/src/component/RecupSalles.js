import React, { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';

export default function RecupSalles() {
    const [pret, setPret] = useState([]);
    const [listeSalle, setListeSalle] = useState([]);

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListeSalle);
            setPret(true);
        }
        fetchJson();
        recup(setListeSalle);
    }, []);

    while (listeSalle === []){
        recup(setListeSalle);
    }

   

    return <Fragment>
        {
            pret ? (
                <div className="Container">
                    <div className="card-deck">
                        {recupListe(listeSalle)}
                    </div>
                </div>
            ) : (<div></div>)}

    </Fragment>
}

function recupListe(listeSalle) {
    return (
        <div>
            {listeSalle.map((salle) =>
                 <div className="card" key={salle.id}>
                    
                    <div className="card-body">
                        <h5 className="card-title">{salle.nom}</h5>
                        <Link to={`/VoirSalle/${salle.id}`} >
                          <button className="btn btn-primary">voir</button>
                        </Link>
                        <Link to={`/ModifierSalle/${salle.id}`} >
                          <button className="btn btn-light">Modifier</button>
                        </Link>
                        <input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteSalle(salle.id)}} />
                    </div>
                 </div>
            )}      
        </div>
    );
}

async function recup(setListeSalle) {
    await fetch(`${process.env.REACT_APP_API_URL}/salles`, {
        method: "get",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
        .then(response =>{ return response.json()})
        .then(data => {
            setListeSalle(data);
        })
        .catch(error => { console.log(error); })
}

function deleteSalle(id) {
    fetch(`${process.env.REACT_APP_API_URL}/salle/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/Salle";

}

function voirSalle(id){
    
}