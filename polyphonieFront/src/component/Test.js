import React, { useState, useEffect } from "react";



export default function ListerMessages() {

    const [messages, setMessages] = useState([]);

    const getMessages = () => {

        fetch(`${process.env.REACT_APP_API_URL}/MSGliste`, {
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
             })

            .then(res => {
                return res.json();
            })

            .then(data => {

                setMessages(data);
                console.log(messages)
            })

            .catch(error => {
                console.log(error);

            })
    }
    useEffect(() => {
        
        const fetchData = async () => {
      
            await getMessages();
        }
        fetchData()
    }, []);

    