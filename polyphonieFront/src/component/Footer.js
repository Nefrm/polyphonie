import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter, MDBIcon} from "mdbreact";

const Footer = () => {
  return (
    <MDBFooter className="font-small pt-4 mt-4">
    <MDBContainer fluid className="text-center text-md-left">
      <MDBRow>
        <MDBCol md="6">
      
          <div>
              <img src={require('../images/logo.png')}/>
            
          </div>
        </MDBCol>
        <MDBCol md="6">
          <h5 className="title">Conservatoire Polyphonie Lille</h5>
          <ul>
            <li className="list-unstyled">
              <a >20 Rue de la musique</a>
            </li>
            <li className="list-unstyled">
              <a >59310 La Madeleine</a>
            </li>
            <li className="list-unstyled">
              <a >Lille</a>
            </li>
           
          </ul>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
    <div className="footer-copyright text-center py-3">
      <MDBContainer fluid>
        &copy; {new Date().getFullYear()} Copyright: <a href="https://www.mdbootstrap.com"> Polyphonie.com </a>
      </MDBContainer>
    </div>
  </MDBFooter>
  );
}

export default Footer;