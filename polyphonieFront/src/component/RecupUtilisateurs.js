import React, { useState, useEffect, Fragment } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function RecupUtilisateurs() {
    const [pret, setPret] = useState([]);
    const [liste, setListe] = useState([]);

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListe);
            setPret(true);
        }
        fetchJson();
        recup(setListe);
    }, []);
    
    /*while (liste == []){
        recup(setListe);
    }*/
    return <Fragment>
        {
            pret ? (
                <div className="Container">
                    <div className="row col-md-12 col-md-offset-2 ">
                        <table className=" table table-striped fond">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Nom</th>
                                    <th>Prénom</th>
                                    <th>Date de naissance</th>
                                    <th>Mail</th>
                                    <th>Telephone</th>
                                    <th>Adresse</th>
                                    <th>Rôle</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            {recupListe(liste)}
                        </table>
                    </div>
                </div>
            ) : (<div></div>)}
    </Fragment>
}

function recupListe(liste) {
    return (
        <tbody>
            {liste.map((utilisateur) =>
                <tr key={utilisateur.id}>
                    <td>{utilisateur.nom}</td>
                    <td>{utilisateur.prenom}</td>
                    <td>{utilisateur.dateNaissance}</td>
                    <td>{utilisateur.mail}</td>
                    <td>{utilisateur.telephone}</td>
                    <td>{utilisateur.adresse}</td>
                    <td>{utilisateur.role}</td>
                    <td><Button className="btn btn-primary btn-block">voir</Button></td>
                    
                    <td>
                        <Link to={`/modificationutilisateur?idUser=${utilisateur.id}`} >
                            <input className="btn btn-warning btn-block" type="button" value="Modifier" />
                        </Link>
                        </td>
                    <td><Button onClick={() => supprimerUtilisateur(utilisateur.id)} className="btn btn-danger btn-block">supprimer</Button></td>
                </tr>
            )}
        </tbody>
    );
}

async function recup(setListe) {
    await fetch(`${process.env.REACT_APP_API_URL}/user-autre-infos`, {
        method: "get",
        headers: {
            "Authorization" : `Bearer ${sessionStorage.getItem("token")}`,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
    .then(response => response.json())
        .then(json => {
            setListe(json);
        })
        .catch(error => { console.log(error); })
}

async function supprimerUtilisateur(idUser) {
    await fetch(`${process.env.REACT_APP_API_URL}/user-autre-infos/${idUser}`, {
        method: "delete",
        headers: {
            "Authorization" : `Bearer ${sessionStorage.getItem("token")}` ,
        }
    }).then(() => {
        document.location.reload();
    })
        .catch(error => {
            console.log(error);
        })
}