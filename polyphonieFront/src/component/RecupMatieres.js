import React, { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';

export default function RecupMatieres() {
    const [pret, setPret] = useState([]);
    const [listeMatiere, setListeMatiere] = useState([]);

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListeMatiere);
            setPret(true);
        }
        fetchJson();
        recup(setListeMatiere);
    }, []);

    while (listeSalle === []){
        recup(setListeMatiere);
    }

   

    return <Fragment>
        {
            pret ? (
                <div className="Container">
                    <div className="card-deck">
                        {recupListe(listeMatiere)}
                    </div>
                </div>
            ) : (<div></div>)}

    </Fragment>
}

function recupListe(listeMatiere) {
    return (
        <div>
            {listeMatiere.map((matiere) =>
                 <div className="card" key={matiere.id}>
                    
                    <div className="card-body">
                        <h5 className="card-title">{matiere.libelle}</h5>
                        <Link to={`/VoirMatiere/${matiere.id}`} >
                          <button className="btn btn-primary">voir</button>
                        </Link>
                        <Link to={`/ModifierMatiere/${matiere.id}`} >
                          <button className="btn btn-light">Modifier</button>
                        </Link>
                        <input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteMatiere(matiere.id)}} />
                    </div>
                 </div>
            )}      
        </div>
    );
}

async function recup(setListeSalle) {
    await fetch(`${process.env.REACT_APP_API_URL}/matieres`, {
        method: "get",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
        .then(response =>{ return response.json()})
        .then(data => {
            setListeMatiere(data);
        })
        .catch(error => { console.log(error); })
}

function deleteSalle(id) {
    fetch(`${process.env.REACT_APP_API_URL}/matiere/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/Matiere";

}

function voirMatiere(id){
    
}