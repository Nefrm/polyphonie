import React, { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';

export default function RecupEvenements() {
    const [pret, setPret] = useState([]);
    const [listeEvent, setListeEvent] = useState([]);

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListeEvent);
            setPret(true);
        }
        fetchJson();
        recup(setListeEvent);
    }, []);

    while (listeEvent === []){
        recup(setListeEvent);
    }

   

    return <Fragment>
        {
            pret ? (
                <div className="Container">
                    <div className="card-deck">
                        {recupListe(listeEvent)}
                    </div>
                </div>
            ) : (<div></div>)}

    </Fragment>
}
function changeUrl(string){
    const strTab  = string.split('\\');
    let i = strTab.length;
    const str = strTab[i-1];
    return str;
}
function recupListe(listeEvent) {
    return (
        <div>
            {listeEvent.map((event) =>
                 <div className="card" key={event.id}>
                    <img src={`/images/${changeUrl(event.urlImage)}`} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{event.titre}</h5>
                        <Link to={`/VoirEvenement/${event.id}`} >
                          <button className="btn btn-primary">voir</button>
                        </Link>
                        <Link to={`/ModifierEvenement/${event.id}`} >
                          <button className="btn btn-light">Modifier</button>
                        </Link>
                        <input  type="submit" className="btn btn-danger" name = "supprimer" value="supprimer" onClick={() => {deleteEvenement(event.id)}} />
                    </div>
                 </div>
            )}      
        </div>
    );
}

async function recup(setListeEvent) {
    await fetch(`${process.env.REACT_APP_API_URL}/evenements`, {
        method: "get",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
        .then(response =>{ return response.json()})
        .then(data => {
            setListeEvent(data);
        })
        .catch(error => { console.log(error); })
}

function deleteEvenement(id) {
    fetch(`${process.env.REACT_APP_API_URL}/evenement/${id}`, {
       
        method : 'delete'
         })
        .then(res => {

            if(res.status === 200){

            }
            return res.text();
            
 
        })
        .catch(error => {
            console.log(error);
        })
        window.location.href = "/Evenement";

}

function voirEvenement(id){
    
}