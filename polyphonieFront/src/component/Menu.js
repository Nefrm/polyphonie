import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

    class Menu extends React.Component {
        constructor(props) {
          super(props);
          this.state = {
            collapse: false,
            url : ''
          };
          this.onClick = this.onClick.bind(this);
          
        }
      
        onClick() {
          this.setState({
            collapse: !this.state.collapse,
          });
        }
    
        
    
        render() {
            return (
               
                <nav className="navbar navbar-expand-lg navbar-light bg-dark">
  <a className="navbar-brand" href="/">Polyphonie</a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <a className="nav-link" href="#">Informations <span className="sr-only">(current)</span></a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/Contact">Contact</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/Evenement">Evènement</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/CreateEvenement">Créer un Evènement</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/visualisationutilisateur">Visualiser les Utilisateurs</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/creerutilisateur">Créer un Utilisateur</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/MessagerieAdmin">Messagerie</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/ajouterdocument">Ajouter un Document</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/listerdocument">Lister les Documents</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/reservation">Faire une réservation</a>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="/listerSalle">Lister les Salles</a>
      </li>
      <div className="form-inline my-2 my-lg-0">
      
      <a href="/connexion"><i className="fa fa-power-off green-text"> Connexion</i></a>
    </div>
    </ul>
    
  </div>
</nav>

                )
        }
    }
    
    export default Menu