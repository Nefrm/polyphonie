import React from "react";
import { MDBCarousel, MDBCarouselCaption, MDBCarouselInner, MDBCarouselItem, MDBView, MDBMask, MDBContainer } from
"mdbreact";


const Car = () => {
  return (
      <div className="car">
          
    <MDBContainer>
      <MDBCarousel
      activeItem={1}
      length={3}
      showControls={true}
      showIndicators={true}
      className="z-depth-1"
    >
      <MDBCarouselInner>
        <MDBCarouselItem itemId="1">
          <MDBView>
            <img
              className="d-block w-100"
              src={require('../images/piano.jpg')}
              alt="First slide"
            />
          <MDBMask overlay="white-slight" />
          </MDBView>
          <MDBCarouselCaption>
            <h3 className="h3-responsive">Cours de piano</h3>
            <p>Niveau débutant ou confirmé</p>
          </MDBCarouselCaption>
        </MDBCarouselItem>
        <MDBCarouselItem itemId="2">
          <MDBView>
            <img
              className="d-block w-100"
              src={require('../images/saxo.jpg')}
              alt="Second slide"
            />
          <MDBMask overlay="white-slight" />
          </MDBView>
          <MDBCarouselCaption>
            <h3 className="h3-responsive">Cours de saxophone</h3>
            <p>Niveau débutant ou confirmé</p>
          </MDBCarouselCaption>
        </MDBCarouselItem>
        <MDBCarouselItem itemId="3">
          <MDBView>
            <img
              className="d-block w-100"
              src={require('../images/micro.jpg')}
              alt="Third slide"
            />
          <MDBMask overlay="white-slight" />
          </MDBView>
          <MDBCarouselCaption>
            <h3 className="h3-responsive">Cours de chant</h3>
            <p>Technique de voix, respiration, solfège...</p>
          </MDBCarouselCaption>
        </MDBCarouselItem>
      </MDBCarouselInner>
    </MDBCarousel>
    </MDBContainer>
    
   
   
  
    </div>
  );
}

export default Car;

