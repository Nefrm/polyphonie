import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import Home from '../page/Home';
import Contact from '../page/Contact';
import Evenement from '../page/Evenement';
import CreationUtilisateur from '../page/CreationUtilisateur';
import ModifierEvenement from '../page/ModifierEvenement';
import ModificationUtilisateur from '../page/ModificationUtilisateur';
import ListerUtilisateur from '../page/ListerUtilisateur';
import Message from '../page/Message';
import MessagerieStudent from '../page/MessagerieStudent';
import MessagerieProf from '../page/MessagerieProf';
import MessagerieAdmin from '../page/MessagerieAdmin';
import CreationEvenement from '../page/CreationEvenement';
import CreationDocument from '../page/CreationDocument';
import ListerDocument from '../page/ListerDocument';
import VoirEvenement from '../page/VoirEvenement';
import ListerSalle from '../page/ListerSalle';
import Connexion from '../page/Connexion';
import CreationReservation from '../page/CreationReservation';

// vérifie que l'utilisateur est connecté
const PrivateRoute = ({ component: Component, token, autorise, role, ...rest }) => (
    <Route
      {...rest}
      render={props =>
        (token && token !== "" && role !== "" && role.some((r)=>autorise.includes(r))) ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", }} />
        )
      }
    />
  );

export default function Chemin() {
    let roles
    if(sessionStorage.getItem("authorities") != undefined) {
        roles = sessionStorage.getItem("authorities").split(",")
        console.log(roles)
    }
    return (
        <Router>
            <div>
                <Switch>

                {/*route Admin*/}
                    <PrivateRoute path="/visualisationutilisateur" token={sessionStorage.token} role={roles} autorise={["ROLE_ADMIN"]} component={ListerUtilisateur} />
                    <PrivateRoute path="/modificationutilisateur" token={sessionStorage.token} role={roles} autorise={["ROLE_ADMIN"]} component={ModificationUtilisateur} />
                    <PrivateRoute path="/creerutilisateur" token={sessionStorage.token} role={roles} autorise={["ROLE_ADMIN"]} component={CreationUtilisateur} />
                {/*route Professeur*/}
                    <PrivateRoute path="/ModifierEvenement/:id" token={sessionStorage.token} role={roles} autorise={["ROLE_ADMIN","ROLE_PROFESSEUR"]} component={ModifierEvenement} />
                    <PrivateRoute path="/CreateEvenement" token={sessionStorage.token} role={roles} autorise={["ROLE_ADMIN","ROLE_PROFESSEUR"]} component={CreationEvenement} />
                    <PrivateRoute path="/ajouterdocument" token={sessionStorage.token} role={roles} autorise={["ROLE_ADMIN","ROLE_PROFESSEUR"]} component={CreationDocument} />
                {/*route Eleve*/}
                    <PrivateRoute path="/listerdocument" token={sessionStorage.token} role={roles} autorise={["ROLE_ADMIN","ROLE_ELEVE"]} component={ListerDocument} />
                {/*route Public*/}
                    <Route path="/connexion">
                        <Connexion /> 
                    </Route>
                    <Route path="/contact">
                        <Contact />
                    </Route>
                    <Route path="/Evenement">
                        <Evenement /> 
                    </Route>
                    <Route path="/VoirEvenement/:id">
                        <VoirEvenement /> 
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}